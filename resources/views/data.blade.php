
 @foreach($products  as $product)
                  
                    <div class="col-6 col-md-3 product-details"  data-product_name="{{$product->product_name}}" data-category_name="">
                        <div class="sc-product-item">
                            <div class="card card-poduct d-flex flex-column">
                                       <div id="badge-quantity{{$product->id}}" class="quantity-badge sc-product-item thumbnail sc-added-item text-white d-none">0</div>
                                                        <div class="product-img-wrapper mx-auto">
                                              <a class="fancybox" href="{{ URL('/products/'.$product->product_image)}}" title="{{$product->product_name}}">
                              <img data-name="product_image" @if($product->product_image) src="{{ URL('/products/'.$product->product_image)}}" @else  src="{{ URL('/products/default-image.png')}}" @endif class="product-img" alt="{{$product->product_name}}" title="{{$product->product_name}}">
                                    </a>
                                     </div>
                                 <div class="inner-mod">                                  
                                <div class="product-name">
                                    <p>{{$product->product_name}}</p>
                                </div>
                                <div class="">
                                    <div class="product-price" style="margin-bottom: 5px">
                                        <span >
                                            <strong><span class="price" id="product_price">₹ {{$product->rate}}<span></strong>
                                        </span>
                                        <input id="input-quantity{{$product->id}}" class="sc-cart-item-qty pull-right text-center" name="product_quantity" min="1" value="1" type="hidden">
                                    </div>
                                    <input id="hidden_product_name{{$product->id}}" name="product_name" value="{{$product->product_name}}" type="hidden" />
                                    <input id="hidden_product_price{{$product->id}}" name="product_price" value="{{$product->rate}}" type="hidden" />
                                    <input name="product_id" value="{{$product->id}}" type="hidden" />
                                    <div class="cart-add-btn-wrapper">
                                    <button class="btn btn-success btn-add-cart sc-add-to-cart btn-xs" data-pname="{{$product->product_name}}"> <span><span class="cart-icon"><img src="{{url('front-end/images/cart.png')}}"></span>Add to Cart</span></button>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>                  
                        @endforeach         