<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>WhatsCy</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="icon" href="{{url('images/favicon.png')}}" type="image/x-icon"/>
  <!-- Font Awesome -->
   @include('layouts.front-style')
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

  <!-- summernote -->
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<style>
  .error{
    color: red;
  }
  label,
  input,
  button {
    border: 0;
    margin-bottom: 3px;
    display: block;
    width: 100%;
  }
 .common_box_body {
    padding: 15px;
    border: 12px solid #28BAA2;
    border-color: #28BAA2;
    border-radius: 15px;
    margin-top: 10px;
    background: #d4edda;
}
</style>
<body class="register-page" style="height: 100%; margin: 0">
<div class="wrapper">
  <div class="content-wrapper">
    <div class="container-fluid">

    <div class="whole-divz">
      <div class="left-s">
        <div class="head-secs">
          <h1>Register for an all-inclusive<br>Experience on WhatsCy</h1>
        </div>
        <img src="dist/img/phone-block.png" class="img-fluid d-block mx-auto">
      </div>
      <div class="right-s">
        <div class="right-inner">
        <div class="lo-sec">
          <img src="dist/img/whatscy-blue-logo.png" class="img-fluid mx-auto d-block">
        </div>
        <div class="reg-form">
            <form method="POST" id="register-form" action="{{ route('register') }}">
                @csrf
            <input type="text" class="form-control input-class @error('name') is-invalid @enderror" value="{{ old('name') }}" required  name ="name" placeholder="Full Name">
                                  @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
            <input type="text" class="form-control input-class" name ="email" required placeholder="Email ID">
                               @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
            <input type="text" class="form-control input-class" name ="phone" required placeholder="Mobile number">
                               @error('phone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
            <input type="text" class="form-control input-class" required required name ="user_name" placeholder="Username">
            <input type="password" class="form-control input-class" name ="password" placeholder="Password" autocomplete="new-password">
            <button type="submit" class="submitz">SEND</button>
          </form>
        </div>
      </div>
      <div class="copy">
        <p>All Rights Reserved. 2020.</p>
      </div>
    </div>
  </div>
  </div>
  </div>
</div>

@include('layouts.front-script')

</body>
</html>

<script>
    $(document).ready(function () {
      $.validator.addMethod(
        "regex",
        function(value, element, regexp) {
            var re = new RegExp(regexp);
            return this.optional(element) || re.test(value);
        },
        "Please check your email."
);
    $('#register-form').validate({ 
        rules: {
            name: {
                required: true
            },
            email: {
                required: true,
                email: true,
                 regex: /^\d{5}(?:[-\s]\d{4})?$/
            },
          
            
          
          phone:{
              required: true,
                minlength: 10,
                digits: true
              },
             
             
            password: {
                required: true,
               minlength: 6
                
            },
            user_name:{
                 required: true,
            },
           
           
            
        },
         messages: {
    name: {
      required: "Please enter your name ",
     
    },
 
       phone: {
      required: "Please enter  mobile number",
      },
        user_name: {
      required: "Please enter  a user name",
      },
    
    email: {
      required: "Please enter email",
      email: "Email address must be in the format of name@whatcy.com"
    },
     password: {
      required: "Please enter a password",
      
    }
  }
    });
});
</script>