<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>WhatsCy</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="icon" href="{{url('images/favicon.png')}}" type="image/x-icon"/>
  <!-- Font Awesome -->
  @include('layouts.front-style')
  <!-- summernote -->
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<style>
  .error{
    color: red;
  }
  label,
  input,
  button {
    border: 0;
    margin-bottom: 3px;
    display: block;
    width: 100%;
  }
 .common_box_body {
    padding: 15px;
    border: 12px solid #28BAA2;
    border-color: #28BAA2;
    border-radius: 15px;
    margin-top: 10px;
    background: #d4edda;
}
</style>
<body class="register-page log-page" style="height: 100%; margin: 0">
<div class="wrapper">
  <div class="content-wrapper">
    <div class="container-fluid">

    <div class="whole-divz">
      
      <div class="right-s top-v">
        <div class="right-inner">
        <div class="lo-sec">
          <img src="dist/img/whatscy-blue-logo.png" class="img-fluid mx-auto d-block">
        </div>
        <div class="reg-form">
            <form id="login-form" method="POST" action="{{ route('login') }}">
                @csrf
            <input type="email" class="form-control input-class @error('email') is-invalid @enderror" name ="email" autocomplete="email" autofocus required placeholder="Email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
            <input type="password" class="form-control input-class" name ="password" placeholder="Password">
             @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
          <div class="remember-me">
             <input type="checkbox" name="remember" id="remember" value="lsRememberMe" id="rememberMe"  {{ old('remember') ? 'checked' : '' }} class="checkz"> 
          <labels for="rememberMe" class="labelz">Remember me</label>
            </div>
            <button type="submit" class="submitz">SEND</button>
            <h6>Did you <a href="{{ route('password.request') }}"><span>forget your password?</span></a></h6>
          </form>
        </div>
      </div>
      <div class="copy">
        <p>All Rights Reserved. 2020.</p>
      </div>
    </div>
  </div>
  </div>
  </div>
</div>

<@include('layouts.front-script')
</body>
</html>
<script>
    $(document).ready(function () {
    $('#login-form').validate({ 
        rules: {
           email: {
                required: true
            },
           password: {
                required: true,
              
            },
           
            
        },
         messages: {
    email: {
      required: "Please enter your registerd email ",
     
    },
 
     password: {
      required: "Please enter your  password",
      
    },
  }
    });
});
</script>