
<!DOCTYPE html>


<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <link rel="icon" href="{{url('images/favicon.png')}}" type="image/x-icon"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
       <meta name="csrf_token" content="{{ csrf_token() }}">
    <meta name="title" property="title" content="test">
    <meta http-equiv="refresh" content="3600" />
    <title>{{$shop['shop_name']}}</title>

    
    <!-- Google Analytics -->
  
    <!-- End Google Analytics -->
 <link rel="stylesheet" href="{{url('front-end/css/front.css')}}">
   <link rel="stylesheet" href="{{url('front-end/css/cart.css')}}">
     <link rel="stylesheet" href="{{url('front-end/css/toast.css')}}">
       <link rel="stylesheet" href="{{url('front-end/css/snackbar.css')}}">
       <link rel="stylesheet" href="{{url('front-end/css/custom.css')}}">
       
    <!-- bootsrap 4 -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <!-- fontawesome script -->
    <script src="https://kit.fontawesome.com/07b5656a8a.js" crossorigin="anonymous"></script>

    <!-- google roboto font family -->
    <link href="https://fonts.googleapis.com/css2?family=Roboto+Condensed:ital,wght@0,300;0,400;0,700;1,300;1,400&display=swap" rel="stylesheet">

    <!-- fancy-box image viwer -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />

   
    <style>
        /* header css */
        header {
            background-color: #7f00ff !important;
        }
        .brand-name {
            color: #fff;
            font-size: 2em;
        }
        .footer{
           background-color: #000 !important;
        }
        
            </style>

    <!-- custom css -->
 
</head>
<body>
  <div class="heading-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <img src="{{url('shops/'.$shop['shop_logo'])}}" class="img-fluid">
                    </div>
                    <div class="col-md-9">
                        <div id="search">
                                <label for="search-input"><img src="{{url('front-end/images/search-icon.svg')}}"><span class="sr-only">Search icons</span></label>
                                <input id="search-input" onkeyup="search()" class="form-control input-lg" placeholder="Search" autocomplete="off" spellcheck="false" autocorrect="off" tabindex="1">
                                <!-- <a id="search-clear" href="#" class="fa fa-times-circle hide" aria-hidden="true"><span class="sr-only">Clear search</span></a> -->
                            </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="below-head">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 left-text">
                        @if($shop['shop_timings'])
                        <h5>Shop Timing: <span>{{$shop['shop_timings']}}</span></h5>
                        @endif
                    </div>
                    <div class="col-md-6 right-text">
                          @if($shop['tax'])
                        <h5>All Prices are Inclusive of <span>{{$shop['tax']}}% tax</span></h5>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    <!-- end header -->
    <main>
        <!-- start About content -->
               <!--  <section id="about">
            <div class="container text-center">
                <div class="navbar sub">
                    <b>Restaurent Timing 12 to 10 PM&nbsp;</b><div><b>All prices are inclusive of 5 %&nbsp;</b></div>
                </div>
            </div>
        </section> -->
                <!-- start About content -->
        <!-- start search section -->
                    <section class="search-section">
               
            </section>
                <!-- end search section -->
        <!-- start products display -->
        <div class="products">
            <div class="container">
                <div class="row" id="post-data">
                    <p id="noresult" class="w-100 text-center text-muted d-none"></p>
                  
                
                  
          
     @foreach($products  as $product)
                  
                    <div class="col-6 col-md-3 product-details"  data-product_name="{{$product->product_name}}" data-category_name="">
                        <div class="sc-product-item">
                            <div class="card card-poduct d-flex flex-column">
                                       <div id="badge-quantity{{$product->id}}" class="quantity-badge sc-product-item thumbnail sc-added-item text-white d-none">0</div>
                                                        <div class="product-img-wrapper mx-auto">
                                              <a class="fancybox" href="{{ URL('/products/'.$product->product_image)}}" title="{{$product->product_name}}">
                              <img data-name="product_image" @if($product->product_image) src="{{ URL('/products/'.$product->product_image)}}" @else  src="{{ URL('/products/default-image.png')}}" @endif class="product-img" alt="{{$product->product_name}}" title="{{$product->product_name}}">
                                    </a>
                                     </div>
                                 <div class="inner-mod">                                  
                                <div class="product-name">
                                    <p>{{$product->product_name}}</p>
                                </div>
                                <div class="">
                                    <div class="product-price" style="margin-bottom: 5px">
                                        <span >
                                            <strong><span class="price" id="product_price">₹ {{$product->rate}}<span></strong>
                                        </span>
                                        <input id="input-quantity{{$product->id}}" class="sc-cart-item-qty pull-right text-center" name="product_quantity" min="1" value="1" type="hidden">
                                    </div>
                                    <input id="hidden_product_name{{$product->id}}" name="product_name" value="{{$product->product_name}}" type="hidden" />
                                    <input id="hidden_product_price{{$product->id}}" name="product_price" value="{{$product->rate}}" type="hidden" />
                                    <input name="product_id" value="{{$product->id}}" type="hidden" />
                                    <div class="cart-add-btn-wrapper">
                                    <button class="btn btn-success btn-add-cart sc-add-to-cart btn-xs" data-pname="{{$product->product_name}}"> <span><span class="cart-icon"><img src="{{url('front-end/images/cart.png')}}"></span>Add to Cart</span></button>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>                  
                        @endforeach  
  
 
                                    
                              
                                    </div>
            </div>
        </div>

        <!-- end products display -->
    </main>

    <!-- start cart view button -->
    <button class="btn float-btn" data-toggle="modal" data-target="#checkout">
        <div class="flot-icon-wrapper">
            <div class="badge-wrapper d-none">
                <span  class="bg-info d-flex justify-content-center align-items-center sc-cart-count">0</span>
            </div>
            <i class="fa fa-shopping-cart my-float"></i>
        </div>
    </button>
    <!-- end cart view button -->

    <!-- start Checkout Modal -->
    <div class="modal" id="checkout" tabindex="-1" role="dialog" aria-labelledby="checkoutLabel"  aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header" style="background-color:#FF6C2F">
                    <h5 class="modal-title font-weight-400" id="checkoutLabel">Checkout</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <input type="hidden" value="{&quot;active&quot;:false,&quot;charge&quot;:0,&quot;message&quot;:&quot;Delivery Charge&quot;,&quot;threshold&quot;:-1,&quot;options&quot;:[&quot;Home Delivery&quot;]}" id="options">
                <div class="modal-body">
                    <div class="customer-details">
                        <div class="container">
                            <div class="row">
                                <div id="smartcart"></div>
                            </div>
                                                                                            <div class="row">
                                    <div class="col-12">
                                        <div class="switch-fields">
                                               <input type="radio" id="Take Away" name="pickup" class="delivery-options" value="1" data-value="Take Away" checked/>
                                                    <label for="Take Away">
                                                        <span>Take Away</span>
                                                    </label>
                                                     <input type="radio" id="Home Delivery" name="pickup" class="delivery-options" value="1" data-value="Home Delivery" />
                                                    <label for="Home Delivery">
                                                        <span>Home Delivery</span>
                                                    </label>
                                                    </div>
                                    </div>
                                </div>
                            
                            <div class="row">
                                <input id="extra_charge" type="hidden" />
                                <input id="phone" value="{{$shop->whatsapp_no}}" type="hidden" />
                                <input id="identifier" value="" type="hidden" />
                                <input id="uid" value="{{$shop->id}}" type="hidden" />
                                 <div class="col-12">
                                    <div class="form-group">
                                        <label for="mobile">Name</label>
                                        <input type="text" id="cname" class="form-control"  required placeholder="Enter Your Name">
                              
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="mobile">Mobile Number</label>
                                        <input type="text" id="cphone" class="form-control" id="mobile" placeholder="Enter Your Mobile Number">
                                        <span id="error-phone" style="color: red"></span>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="address">Address</label>
                                        <textarea name="address" class="form-control" id="address" placeholder="Enter Name/Address" rows="5"></textarea>
                                                                            </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-dark btn-modal-close" data-dismiss="modal">Close</button>
                    <button type="button" id="worder" class="btn btn-success  d-flex align-items-center justify-content-around"><i class="fab fa-whatsapp fa-2x"></i> <span>&nbsp;Order on WhatsApp</span></button>
                </div>
            </div>
        </div>
    </div>
    <!-- end Checkout Modal -->

<!-- snack bar tag -->
<snackbar></snackbar>

<!-- start  footer section-->
<footer>
    <div class="container">
        <p style="color: #000">All Rights Reserved. 2020.</p>
    </div>   
</footer>
<!-- start  footer section-->

<!-- jQuery script -->
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>

<!-- bootsrap popper.js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

<!-- bootsrap 4 -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<!-- fancybox image viwer script -->
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>

<!--Floating WhatsApp javascript-->
<script type="text/javascript" src="https://rawcdn.githack.com/rafaelbotazini/floating-whatsapp/3d18b26d5c7d430a1ab0b664f8ca6b69014aed68/floating-wpp.min.js"></script>
<!-- smartcart script -->
<script type="text/javascript" src="{{url('front-end/js/cart.js')}}"></script>

<script src="{{url('front-end/js/snackbar.js')}}"></script>

<!-- Toast script -->
<script src="{{url('front-end/js/toast.js')}}"></script>

<script>
    $(document).ready(function() {
        var options = (JSON.parse($('#options').val()));
        var extra_charge = options;
        $('#smartcart').smartCart({
            currencySettings: {
                locales: 'en-US',
                decimal: 3,
                currencyOptions:  {
                    style: 'currency', 
                    currency: 'INR', 
                    currencyDisplay: 'symbol',
                    minimumFractionDigits: 2,
                    maximumFractionDigits: 2
                }
            },
            lang: {  // Language variables
                cartTitle: "Shopping Cart",
                checkout: 'Checkout',
                clear: 'Clear Cart',
                extra_charge: extra_charge,
                subtotal: 'Subtotal:',
                cartRemove:'<i class="fa fa-trash" aria-hidden="true"></i>',
                cartEmpty: 'Cart is Empty!<br />Choose your products'
            },
        });
        $(".fancybox").fancybox();
    });
</script>
<script type="text/javascript">

    var phone = false;
    var arr = [ 8, 10, 0, 9];


    function urlencode(str) {
        str = (str + '');
        return encodeURIComponent(str)
        .replace(/!/g, '%21')
        .replace(/'/g, '%27')
        .replace(/\(/g, '%28')
        .replace(/\)/g, '%29')
        .replace(/\*/g, '%2A')
        .replace(/~/g, '%7E')
        .replace(/%20/g, '+')
    } // end of urlencode

    $('.sc-add-to-cart').click(function() {
        var product = $(this).attr('data-pname');
        var msg = product+" added to cart successfully";
        // const snackbar  = new SnackBar;
        // snackbar.make("message",[
        //     msg,
        //     null,
        //     "top",
        //     "center"
        //     ], 5000);
        $.toast({
            heading: msg,
            position: 'top-center',
            textAlign: 'center',
            stack: false
        });
    });

    $('#cphone').blur(function(e) {
        phone = $(this).val();
        phone = phone.replace(/[^0-9]/g,'');
        if ($.inArray(phone.length, arr) == -1) {
            $('#error-phone').text('Enter valid phone number');
        } else {
            $('#error-phone').text('');
            phone = true;
        }
    });

    $(document).ready(function() {
        $('#worder').click(function() {

            var items = JSON.parse($("#cart_list").val());
            var itemx = "";
            for(var i=0; i<items.length; i++) {
                itemx = itemx + $.trim(items[i].product_name) + " x " + items[i].product_quantity + "\n";
            }

                            var tot="\nTotal : *" + $('.sc-cart-subtotal').text() + "*\n";
                        
            var address=$.trim($('#address').val());
             var name=$.trim($('#cname').val());

                            var note = "";
                var order_note = "";
            
            var phone=$('#phone').val();
            var extra_charge=$('#extra_charge').val();
            var cphone=$('#cphone').val().replace(/[^0-9]/g,'');

            var identifier=$('#identifier').val();

            var uid=$('#uid').val();
            var pickup=$('input[name="pickup"]:checked').val();

            if (identifier=="") {
                if (pickup==1) {
                    var option = $('input[name="pickup"]:checked').data('value');
                    var mode = "*(_" + option + "_)*";
                } else {
                    var option = "";
                    var mode = "";
                }
            } else {
                var option = identifier;
                var mode = "*(_" + identifier + "_)*";
            }
            

            if((itemx=='')) {
                // const snackbar  = new SnackBar;
                // snackbar.make("message",[
                //     "Your cart is empty",
                //     null,
                //     "top",
                //     "center"
                // ], 5000);

                $.toast({
                    heading: 'Your cart is empty',
                    position: 'top-center',
                    textAlign: 'center',
                    stack: false
                });
            } else if($.inArray(cphone.length, arr) == -1) {
                // const snackbar  = new SnackBar;
                // snackbar.make("message",[
                //     "Enter valid mobile number",
                //     null,
                //     "top",
                //     "center"
                //     ], 5000);
                $.toast({
                    heading: 'Enter valid mobile number',
                    position: 'top-center',
                    textAlign: 'center',
                    stack: false
                });
            } else if((address=='')) {

                $('#address').focus();
                $('#address').css({ 'border-style': 'soild' });
                // const snackbar  = new SnackBar;
                // snackbar.make("message",[
                //     "Enter Address",
                //     null,
                //     "top",
                //     "center"
                // ], 5000);
                $.toast({
                    heading: 'Enter Address',
                    position: 'top-center',
                    textAlign: 'center',
                    stack: false
                });
            }

      else if((name=='')) {

                $('#cname').focus();
                $('#cname').css({ 'border-style': 'soild' });
                // const snackbar  = new SnackBar;
                // snackbar.make("message",[
                //     "Enter Address",
                //     null,
                //     "top",
                //     "center"
                // ], 5000);
                $.toast({
                    heading: 'Enter Name',
                    position: 'top-center',
                    textAlign: 'center',
                    stack: false
                });
            }

             else {
                 localStorage.setItem("name", name);
                localStorage.setItem("mobile", cphone);
                localStorage.setItem("address", address);
                var csrfToken = $('[name="csrf_token"]').attr('content');
     
                var total = $('.sc-cart-subtotal').text();
                var data = {
                    items   : items,
                    pickup  : option, 
                    address : address,
                    note    : note,
                    name    : name,
                    phone   : cphone,
                    extra_charge : extra_charge,
                    uid     : uid, 
                    total   : $.trim(total),
                    _token  : csrfToken,
                }

                var text = mode + "\n\n" + itemx + tot  + "\n" + order_note + note +"\n*_Name:_*\n" + name + "\n*_Address:_*\n" + address + "\n" + cphone + "\n\nPlease confirm before deliver.";
                 var link = urlencode("*New Order From WhatCy : " +" "+ "*\n" + text);
                 
                   $.ajax({
                    method: 'POST',
                    url:"{{ route('ajaxRequest.post') }}",
                    data: data,
                    success: function(data) {
                     
                        var link = urlencode("*New Order From Whatcy : #" + (data.user_token) + "*\n" + text);
                        if(data.user_token)
                        {
                             redirect(phone, link);
                        }
                        
                    },
                    error: function(e) { 
                        console.log(e);
                        const snackbar  = new SnackBar;
                        snackbar.make("message",[
                            "Your session has expired please refresh and try again.",
                            null,
                            "top",
                            "center"
                        ], 10000);

                        
                    }
                });
           
            }
        });
    });

    function redirect(phone, text) {
        url = "https://api.whatsapp.com/send?phone="+ phone + "&text=" + text;
        // window.open(url, '_blank');
        window.location.assign(url);
    }
</script>
<!-- start update cart -->
<script>
    function changeCartCount(){
        let cartList = JSON.parse($('#cart_list').val());
        if(cartList.length == 0){
            $('.badge-wrapper').addClass('d-none');
        }else{
            $('.badge-wrapper').removeClass('d-none');
        }
    }

    function updateCart() {
        let cartList = JSON.parse($('#cart_list').val());
        changeCartCount();
        $.each(cartList, function(index, value) {
            $('#badge-quantity'+value.product_id).html(value.product_quantity);
            if (value.product_quantity > 0) {
                $('#badge-quantity' + value.product_id).removeClass('d-none');
            } else {
                $('#badge-quantity' + value.product_id).addClass('d-none');
            }
        });                
    }
</script>
<!-- end update cart -->

<!-- start search script -->
<script>

    function search() {
        $('#about').hide();
        $('.category-link').removeClass('active');
        $('.all-category').addClass('active');
        var input = $('#search-input').val().toString().toUpperCase();

        var products = $('.product-details');
        var count = products.length;

        products.each(function(i, obj) {

            var value = $(this).data('product_name').toString().toUpperCase();
             
            if(value.indexOf(input) > -1) {
                $(this).fadeIn();
            }else{
                count--
                $(this).fadeOut();
            }
        });
        if(input.length === 0){
            $('#about').fadeIn();
        }
        if(count === 0){
            $('#noresult').removeClass('d-none');
            $('#noresult').removeClass('d-none');
            $('#noresult').html('<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> No result found');
        }else{
            $('#noresult').empty();
            $('#noresult').addClass('d-none');
        }
    }

    // clear search field
    $('#search-clear').click(function(){
        $('#search-input').val('');
        search();
    });

</script>
<!-- end search script -->

<!-- start get values from local storage -->
<script>

    $(function() {
        let mobile = localStorage.getItem('mobile');
        let address = localStorage.getItem('address');
        $('#cphone').val(mobile);
        $('#address').val(address);
    });

</script>
<!-- end get values from local storage -->

<script type="text/javascript">

    var csrfToken = $('[name="csrf_token"]').attr('content');

    setInterval(refreshToken, 900000); // 1 hour 

    function refreshToken(){
        $.get('test/kolayamal/refresh-csrf').done(function(data){
            csrfToken = data; // the new token
        });
        console.log('done');
    }
    setInterval(refreshToken, 900000); // 1 hour 

</script>
<script>

    $('.variation').change(function(){
        var option = $(this).find(':selected');
        let variant_price = option.data('price');
        let id = option.data('product_id');
        $('#product_price'+id).html(variant_price);
        $('#hidden_product_price'+id).val(variant_price);
        let product_name = $('#hidden_product_name'+id).val(option.data('variant'));
    });
</script>
</body>
</html>

<script type="text/javascript">
    var page = 1;
    $(window).scroll(function() {
        if($(window).scrollTop() + $(window).height() >= $(document).height()) {
            page++;
            loadMoreData(page);
        }
    });


    function loadMoreData(page){
      $.ajax(
            {
                url: '?page=' + page,
                type: "get",
                beforeSend: function()
                {
                    $('.ajax-load').show();
                }
            })
            .done(function(data)
            {
                if(data.html == " "){
                    $('.ajax-load').html("No more records found");
                    return;
                }
                $('.ajax-load').hide();
                $("#post-data").append(data.html);
            })
            .fail(function(jqXHR, ajaxOptions, thrownError)
            {
                  alert('server not responding...');
            });
    }
</script>