 <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index.html" class="brand-link" style="background: #056BAA !important;">
      <img src="{{url('dist/img/logo-whatscy.png')}}" alt="AdminLTE Logo" class="">
      <!-- <span class="brand-text font-weight-light">AdminLTE 3</span> -->
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="pages/widgets.html" class="nav-link">
              <i class="nav-icon fas fa-home"></i>
              <p>
                Dashboards
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="pages/widgets.html" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Products
              </p>
            </a>
          </li>
         
          <li class="nav-item">
            <a href="pages/widgets.html" class="nav-link">
              <i class="nav-icon fas fa-plus"></i>
              <p>
                Add Product
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="pages/widgets.html" class="nav-link">
              <i class="fa fa-bar-chart" aria-hidden="true"></i>
              <i class="nav-icon fa fa-bar-chart"></i>
              <p>
                Orders
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="pages/widgets.html" class="nav-link">
              <i class="fa fa-bar-chart" aria-hidden="true"></i>
              <i class="nav-icon fa fa-users"></i>
              <p>
                Customers
              </p>
            </a>
          </li>
          <li class="nav-item" style="background: #E4E4E4;">
            <a href="pages/widgets.html" class="nav-link">
              <p>
                Settings
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="pages/widgets.html" class="nav-link">
              <i class="fa fa-bar-chart" aria-hidden="true"></i>
              <i class="nav-icon fa fa-cog"></i>
              <p>
                Account Settings
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="pages/widgets.html" class="nav-link">
              <i class="fa fa-bar-chart" aria-hidden="true"></i>
              <i class="nav-icon fa fa-shopping-basket"></i>
              <p>
                Shop Details
              </p>
            </a>
          </li>
          <li class="nav-item whatcyy">
            <a href="pages/widgets.html" class="nav-link">
              <i class="fa fa-bar-chart" aria-hidden="true"></i>
              <i class="nav-icon fa fa-shopping-cart"></i>
              <p>
                My WhatsCy
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>