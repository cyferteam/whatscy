 <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{url('dashboard')}}" class="brand-link" style="background: #056BAA !important;">
       <img src="{{url('dist/img/logo-whatscy.png')}}" alt="AdminLTE Logo" class="">
      <!-- <span class="brand-text font-weight-light">AdminLTE 3</span> -->
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item {{ (request()->is('dashboard')) ? 'actives' : '' }}">
            <a href="{{url('dashboard')}}" class="nav-link">
              <i class="nav-icon fas fa-home "></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          @if( $shop_status==1)
          <li class="nav-item {{ (request()->is('dashboard/products')) ? 'actives' : '' }}">
            <a href="{{url('dashboard/products')}}" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Products 
              </p>
            </a>
          </li>
         
          <li class="nav-item {{ (request()->is('dashboard/add-products')) ? 'actives' : '' }}">
            <a href="{{url('dashboard/add-products')}}" class="nav-link">
              <i class="nav-icon fas fa-plus"></i>
              <p>
                Add Product
              </p>
            </a>
          </li>
          <li class="nav-item {{ (request()->is('dashboard/orders')) ? 'actives' : '' }}">
            <a href="{{url('dashboard/orders')}}" class="nav-link">
              <i class="fa fa-bar-chart" aria-hidden="true"></i>
              <i class="nav-icon fa fa-shopping-bag"></i>
              <p>
                Orders
              </p>
            </a>
          </li>
          <li class="nav-item {{ (request()->is('dashboard/customers')) ? 'actives' : '' }}">
            <a href="{{url('dashboard/customers')}}" class="nav-link">
              <i class="fa fa-bar-chart" aria-hidden="true"></i>
              <i class="nav-icon fa fa-users"></i>
              <p>
                Customers
              </p>
            </a>
          </li>
          @endif
          <li class="nav-item" style="background: #E4E4E4;">
            <a href="#" class="nav-link">
              <p>
                Settings
              </p>
            </a>
          </li>
          <li class="nav-item {{ (request()->is('dashboard/account-settings')) ? 'actives' : '' }}">
            <a href="{{url('dashboard/account-settings')}}" class="nav-link">
              <i class="fa fa-bar-chart" aria-hidden="true"></i>
              <i class="nav-icon fa fa-cog"></i>
              <p>
                Account Settings
              </p>
            </a>
          </li>
         <!--  <li class="nav-item {{ (request()->is('dashboard/shop-details')) ? 'actives' : '' }}">
            <a href="{{url('dashboard/shop-details')}}" class="nav-link">
              <i class="fa fa-bar-chart" aria-hidden="true"></i>
              <i class="nav-icon fa fa-shopping-basket"></i>
              <p>
                Shop Details
              </p>
            </a>
          </li> -->
                 @if( $shop_status==1)
            <?php $shop=App\User::find(Auth::user()->id)->shop ;
            $shop_url=URL::to($shop['shop_url']);

            ?>
          <li class="nav-item whatcy">
            <a href="<?php echo  $shop_url;?>" target="_blank" class="nav-link">
              <i class="fa fa-bar-chart" aria-hidden="true"></i>
              <i class="nav-icon fa fa-shopping-cart"></i>
              <p>
              
              
                My WhatsCy
              </p>
            </a>
          </li>
          @endif
           <li class="nav-item">
            <a href="{{url('logout')}}" class="nav-link">
              <i class="fa fa-bar-chart" aria-hidden="true"></i>
              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p>
                Logout
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>