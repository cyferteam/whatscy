 <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{url('admin/home')}}" class="brand-link" style="background: #056BAA !important;">
       <img src="{{url('dist/img/logo-whatscy.png')}}" alt="AdminLTE Logo" class="">
      <!-- <span class="brand-text font-weight-light">AdminLTE 3</span> -->
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item {{ (request()->is('admin/home')) ? 'actives' : '' }}">
            <a href="{{url('admin/home')}}" class="nav-link">
              <i class="nav-icon fas fa-home "></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
    
          <li class="nav-item {{ (request()->is('admin/shops')) ? 'actives' : '' }}">
            <a href="{{url('/admin/shops')}}" class="nav-link">
                <i class="nav-icon fa fa-shopping-bag"></i> 
              <p>
              Shops
              </p>
            </a>
          </li>
         
          <li class="nav-item {{ (request()->is('admin/add-admin')) ? 'actives' : '' }}">
            <a href="{{url('admin/add-admin')}}" class="nav-link">
              <i class="nav-icon fas fa-plus"></i>
              <p>
                Add Admin
              </p>
            </a>
          </li>
           <li class="nav-item {{ (request()->is('admin/manage-admin')) ? 'actives' : '' }}">
            <a href="{{url('admin/manage-admin')}}" class="nav-link">
              <i class="nav-icon fas fa-plus"></i>
              <p>
                Manage Admin
              </p>
            </a>
          </li>
           <li class="nav-item {{ (request()->is('admin/registered-users')) ? 'actives' : '' }}">
            <a href="{{url('admin/registered-users')}}" class="nav-link">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Users
              </p>
            </a>
          </li>
          <li class="nav-item {{ (request()->is('admin/plans')) ? 'actives' : '' }}">
            <a href="{{url('admin/plans')}}" class="nav-link">
              <i class="fa fa-bar-chart" aria-hidden="true"></i>
               <i class="nav-icon fas fa-th"></i>
              <p>
              Plans
              </p>
            </a>
          </li>
           <li class="nav-item {{ (request()->is('admin/getAllSubscription')) ? 'actives' : '' }}">
            <a href="{{url('admin/getAllSubscription')}}" class="nav-link">
              <i class="fa fa-bar-chart" aria-hidden="true"></i>
               <i class="nav-icon fas fa-money-check-alt"></i>
              <p>
             All Subscriptions
              </p>
            </a>
          </li>
         <!--  <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="fa fa-bar-chart" aria-hidden="true"></i>
              <i class="nav-icon fa fa-users"></i>
              <p>
                Customers
              </p>
            </a>
          </li> -->
       
          <li class="nav-item" style="background: #E4E4E4;">
            <a href="#" class="nav-link">
              <p>
                Settings
              </p>
            </a>
          </li>
          <li class="nav-item {{ (request()->is('admin/profile')) ? 'actives' : '' }}">
            <a href="{{url('admin/profile')}}" class="nav-link">
              <i class="fa fa-bar-chart" aria-hidden="true"></i>
              <i class="nav-icon fa fa-cog"></i>
              <p>
                Profile Settings
              </p>
            </a>
          </li>
         
                
         
      
           <li class="nav-item">
            <a href="{{url('logout')}}" class="nav-link">
              <i class="fa fa-bar-chart" aria-hidden="true"></i>
                  <i class="nav-icon fas fa-sign-out-alt"></i>
              <p>
                Logout
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>