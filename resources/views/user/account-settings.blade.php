<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>WhatsCy</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="icon" href="{{url('images/favicon.png')}}" type="image/x-icon"/>
  <!-- Font Awesome -->
    @include('layouts.stylesheet')
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
 @include('layouts.navbar')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
 
      <!-- Sidebar user panel (optional) -->
      

      <!-- Sidebar Menu -->
       @include('layouts.user-side-menu')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
      <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Account Settings</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
              <li class="breadcrumb-item active">Account Settings</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
       @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
        <!-- Small boxes (Stat box) -->
         @if(Session::has('error'))
        
        <div id="alertmsg" class="alert alert-dismissable alert-danger">
              <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button> 
              <strong>Error</strong>: {{ Session::get('error') }}
          </div>
    @endif
      @if ($message = Session::get('success'))
     
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
    </div>
    <br>
    @endif
    </section>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content headzz">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-8 offset-md-2">
            <div class="tabz">
           <ul class="nav nav-tabs">
  <li class="nav-item">
    <a class="nav-link active" data-toggle="tab" href="#home">Edit Shop</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#menu2">Choose Plan</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#menu1">Change Password</a>
  </li>
  
</ul>

<!-- Tab panes -->
<div class="tab-content">
 
  <div class="tab-pane container active account-set" id="home">
     <form id="add-shop-form" method="POST" action="{{url('add-shop-detials')}}" accept-charset="utf-8" enctype="multipart/form-data" >
      @csrf
    <div class="row">
      <div class="col-md-6">
        <p><label>Shop Name</label></p>
        <input type="text" class="inpp" name="shop_name" value="@if($shop) {{$shop->shop_name}} @endif" placeholder="Shop Name">
      </div>
      <div class="col-md-6">
        <p><label>Shop URL</label></p>
        @if($shop)
           <input type="text" class="inpp" name="shop_url" value="@if($shop){{$shop->shop_url}}@endif" disabled  placeholder="Shop URL">
        @else
           <input type="text" class="inpp" name="shop_url" value="@if($shop){{$shop->shop_url}}@endif" placeholder="Shop URL">
        @endif
     
      </div>
      <div class="col-md-6">
        <p><label>Whatsapp No</label></p>
        <?php if($shop){ $number= ltrim($shop->whatsapp_no, '+');}  ?>
        <input type="text" class="inpp" name="whatsapp_no" value="@if($shop){{$number}}@endif"  placeholder="Whatsapp No.">
        <span>Format: 91xxxxxxxxxx (10 digit WhatsApp number with country code)</span>
      </div>
      <div class="col-md-6">
        <p><label>Shop Logo</label></p>
        <input type="file" class="inpp filez" name="shop_logo" placeholder="Shop Logo">
        <span>Dimension: 150x60; Types: png, jpg, jpeg; Max Size: 1MB;</span>
      </div>
       <div class="col-md-6">
        <p><label>Shop Timing</label></p>
        <input type="text" class="inpp" name="shop_timing" value="@if($shop) {{$shop->shop_timings}} @endif" placeholder="Shop Name">
        <span>Eg: 10 AM to 6 PM</span>
      </div>
       <div class="col-md-6">
        <p><label>Tax %</label></p>
         <input type="text" class="inpp" name="tax" value="@if($shop){{$shop->tax}}@endif" placeholder="Tax">
       
      </div>
    </div>
  <div class="button-area">
    <button type="submit" class="subz">Update now</button>
  </div>
 </form>
  </div>

  <div class="tab-pane container fade second-tab account-set" id="menu2">
    @if($subscription)
    <p>Your current plan is {{$subscription->plan_name}}</p><br>
        <p>Your  plan status is {{$result['status']}}</p><br>
    <p>Plan started at :  <?php 
                     echo date('M/d/Y', $result['current_start']); 
                   ?> </p><br>
                    <p>Plan ends at :  <?php 
                     echo date('M/d/Y', $result['current_end']); 
                   ?> </p>
                  @if( $result['status']=="active")
                   <div class="button-area">
                    <form ethod="post" action="{{url('/dashboard/unsubscribe-plan')}}">
                      <input type="hidden" name="subscription_id" value="{{$result['id']}}">
    <button type="submit" class="subz">Unsubscribe Plan</button>
  </form>
  </div>
   <div class="button-area">
                    <form ethod="post" action="{{url('/dashboard/update-plan')}}">
                      <input type="hidden" name="subscription_id" value="{{$result['id']}}">
    <button type="submit" class="subz">Upgrade Plan</button>
  </form>
  </div>
  @endif
 

      @else
    <form method="post" action="{{url('/dashboard/choose-plan')}}">
        @csrf
    <div class="row">
      
      <div class="col-md-12">
        <p><label>Select Plan</label></p>
        <select class="inpp" name="plan">
          <option selected disabled>Select Plan</option>
          @foreach($plans as $plan)
          <option value="{{$plan->plan_id}}">{{$plan->plan_name}}</option>
          @endforeach
        </select>
        <span>Select Plan</span>
      </div>
     
    

    </div>

    <div class="button-area">
    <button type="submit" class="subz">Submit</button>
  </div>
</form>
@endif
  </div>

  <div class="tab-pane container fade second-tab account-set" id="menu1">
    <form id="password-form" method="post" action="{{url('/dashboard/update-user-password')}}">
        @csrf
    <div class="row">
      
      <div class="col-md-4">
        <p><label> Current Password</label></p>
        <input id="old_password" type="password" name="old_password" required class="inpp" placeholder="Enter Current Password" autofocus>
        <span>Enter Current Password</span>
      </div>
      <div class="col-md-4">
        <p><label>New Password</label></p>
        <input type="password" id="password" class="inpp" required  name="password" placeholder="Enter Password" autofocus>
        <span>Password length must be 6-15 characters long</span>
      </div>
      <div class="col-md-4">
        <p><label>Re-Enter Password</label></p>
        <input type="password" name="password_confirmation" id="password_confirmation"  required  class="inpp" placeholder="Re-Enter Password" autofocus>
        
      </div>

    </div>
    <div class="button-area">
    <button type="submit" class="subz">Change Password</button>
  </div>
</form>
  </div>
 
</div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
   @include('layouts.footer')

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
@include('layouts.js')
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
@include('layouts.js')
</body>
</html>
<script>
    $(document).ready(function () {
    $('#add-shop-form').validate({ 
        rules: {
          shop_name: {
                required: true,
                required: true,
            },
              tax: {
                  number :true,
            },
          whatsapp_no: {
                required: true,
                number :true,
                   matches   : /^1?(\d{3})(\d{3})(\d{4})$/,

               },
           },
         messages: {
     shop_name: {
      required: "Please enter shop name ",
     
    },
  whatsapp_no: {
      required: "Please enter whatsapp no with country code ",

      },
  }
    });

    //password form

        $('#password-form').validate({ 
        rules: {
          old_password: {
                required: true
            },
          password: {
                  required: true,
                  minlength: 6

            },
          password_confirmation: {
                required: true,
                   minlength: 6
              

               },
           },
         messages: {
     shop_name: {
      required: "Please enter shop name ",
     
    },
  whatsapp_no: {
      required: "Please enter whatsapp no with country code ",

      },
  }
    });
});
</script>

