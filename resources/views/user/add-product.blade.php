<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>WhatsCy</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="icon" href="{{url('images/favicon.png')}}" type="image/x-icon"/>
  <!-- Font Awesome -->
   @include('layouts.stylesheet')
 </head>
 <body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">
    @include('layouts.navbar')
  <!-- Navbar -->
     
  <!-- /.navbar -->

  

     @include('layouts.user-side-menu')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Add Products</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
              <li class="breadcrumb-item active">Add Products</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
       @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
        <!-- Small boxes (Stat box) -->
         @if(Session::has('error'))
        
        <div id="alertmsg" class="alert alert-dismissable alert-danger">
              <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button> 
              <strong>Error</strong>: {{ Session::get('error') }}
          </div>
    @endif
      @if ($message = Session::get('success'))
     
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
    </div>
    <br>
    @endif
    </section>
    <!-- /.content-header -->
    <div class="row"> 
       <div class="col-md-6 offset-md-3">
      <a href="bulk-upload">Bulk upload</a>
    </div>
    </div>
  
    <!-- Main content -->
    <section class="content headzz">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-6 offset-md-3">
            <div class="add-product">
              <form id="add-product-form" class="formz" method="POST" action="{{url('add-product')}}"  accept-charset="utf-8" enctype="multipart/form-data" >
                 @csrf
                <label>Product Name</label>
                <p><input type="text" class="form-control" name="name" placeholder="Product Name"></p>
                <label>Price</label>
                <p><input type="text" class="form-control" name="rate" placeholder="Price"></p>
                <label>Product Image</label>
                <p><input type="file" name="fileUpload"></p>
                <div class="button-save mr-auto">
                  <button class="cancelz" type="reset" >Cancel</button>
                  <button class="savez" type="submit">Save</button>
                </div>
              </form>
            </div>
          </div>
        </div>

       
    </section>

    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
     @include('layouts.footer')

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->
@include('layouts.js')

<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->

<!-- Bootstrap 4 -->

</body>
</html>

<script>
    $(document).ready(function () {
    $('#add-product-form').validate({ 
        rules: {
          name: {
                required: true
            },
          rate: {
                required: true,
                digits: true,
               },
           },
         messages: {
    name: {
      required: "Please enter product name ",
     
    },
   rate: {
      required: "Please enter product price ",

      },
  }
    });
});
</script>
