<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>WhatsCy</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="icon" href="{{url('images/favicon.png')}}" type="image/x-icon"/>
  <!-- Font Awesome -->
   @include('layouts.stylesheet')

 <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.dataTables.min.css">
 </head>

 <body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">
    @include('layouts.navbar')
  <!-- Navbar -->
     
  <!-- /.navbar -->

  

     @include('layouts.user-side-menu')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Customers</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
              <li class="breadcrumb-item active">Customers</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
       @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
        <!-- Small boxes (Stat box) -->
         @if(Session::has('error'))
        
        <div id="alertmsg" class="alert alert-dismissable alert-danger">
              <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button> 
              <strong>Error</strong>: {{ Session::get('error') }}
          </div>
    @endif
      @if ($message = Session::get('success'))
     
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
    </div>
    <br>
    @endif
    </section>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Customers</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="orderTables" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Phone</th>
                 

                    
                </tr>
                </thead>
                <tbody>
                  <?php $a=1?>
            
              @foreach($customers as $customer)
                <tr>
                     <td>{{$a}}</td>
                  <td>{{$customer->c_name}}</td>
               
                  <td>{{$customer->c_phone}}</td>
                 
                </tr>
                <?php $a++; ?>
            @endforeach  
               
                </tbody>
               <!--  <tfoot>
                <tr>
                  <th>#</th>
                  <th>Product Name</th>
                  <th>Rate</th>
                  <th>Image</th>
                    <th>Action</th>
                </tr>
                </tfoot> -->
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->

          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
     @include('layouts.footer')

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->
@include('layouts.js')

<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->

<!-- Bootstrap 4 -->

</body>
</html>
<script>
  $(function () {
   
    $('#orderTables').DataTable({
       responsive: true,
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
     
      "searching": true,

      "responsive": true,
    });
  });
</script>