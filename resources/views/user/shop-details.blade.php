<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>WhatsCy</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="icon" href="{{url('images/favicon.png')}}" type="image/x-icon"/>
  <!-- Font Awesome -->
   @include('layouts.stylesheet')
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
   @include('layouts.navbar')
  <!-- /.navbar -->

 

    <!-- Sidebar -->
      @include('layouts.user-side-menu')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Shop Details</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content headzz">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-8 offset-md-2">
            <div class="tabz">
           <ul class="nav nav-tabs">
  <li class="nav-item">
    <a class="nav-link active" data-toggle="tab" href="#home">Shop Header</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#menu1">Cart</a>
  </li>
  
</ul>

<!-- Tab panes -->
<div class="tab-content">
  <div class="tab-pane container active" id="home">
    <span class="showz">Show logo:</span>
    <label class="radio-button">
    <input type="radio" class="radio-button__input" id="choice1-1" name="choice1">
    <span class="radio-button__control"></span>
    <span class="radio-button__label">Yes</span>
  </label>
  <label class="radio-button">
    <input type="radio" class="radio-button__input" id="choice1-2" name="choice1">
    <span class="radio-button__control"></span>
    <span class="radio-button__label">No</span>
  </label>

  <span class="showz le">Show Title:</span>
    <label class="radio-button">
    <input type="radio" class="radio-button__input" id="choice1-3" name="choice1">
    <span class="radio-button__control"></span>
    <span class="radio-button__label">Yes</span>
  </label>
  <label class="radio-button">
    <input type="radio" class="radio-button__input" id="choice1-4" name="choice1">
    <span class="radio-button__control"></span>
    <span class="radio-button__label">No</span>
  </label>
  <div class="text-sectionz">
    <h5>Write about your shop and services</h5>
    <textarea placeholder="Type your text here" class="textz"></textarea>
    <div class="button-area">
    <button type="submit" class="subz">Update now</button>
  </div>
  </div>
  </div>
  <div class="tab-pane container fade second-tab" id="menu1">
    <h5>Checkout Fields</h5>
    <div class="row">
      <div class="col-md-4">
         <label class="container">Show Order Notes
  <input type="checkbox" checked="checked">
  <span class="checkmark"></span>
</label>
      </div>
      <div class="col-md-8">
        <input type="text" placeholder="Message to be shown on order notes field" class="inputz-text">
      </div>
    </div>
   <div class="checkz-se">
    <h5>Delivery Options</h5>
     <label class="container">Take away
  <input type="checkbox" checked="checked">
  <span class="checkmark"></span>
</label>
<label class="container">Home Delivery
  <input type="checkbox" checked="checked">
  <span class="checkmark"></span>
</label>
<label class="container">Random 1
  <input type="checkbox" checked="checked">
  <span class="checkmark"></span>
</label>
   </div>
   <div class="extra-charge-sec">
      <h5>Extra Charges</h5>
      <label class="container">Activate Etra Charges
  <input type="checkbox" checked="checked">
  <span class="checkmark"></span>
</label>
   </div>
   <div class="button-area">
    <button type="submit" class="subz">Update now</button>
  </div>
  </div>
 
</div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 
     @include('layouts.footer')

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

@include('layouts.js')
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>

</body>
</html>
