<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Product Listing</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="icon" href="{{url('images/favicon.png')}}" type="image/x-icon"/>

  @include('layouts.stylesheet')
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->

    @include('layouts.navbar')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  
    <!-- Sidebar -->
    @include('layouts.user-side-menu')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Product Listing</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
              <li class="breadcrumb-item active">Product Listing</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
       @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
        <!-- Small boxes (Stat box) -->
         @if(Session::has('error'))
        
        <div id="alertmsg" class="alert alert-dismissable alert-danger">
              <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button> 
              <strong>Error</strong>: {{ Session::get('error') }}
          </div>
    @endif
      @if ($message = Session::get('success'))
     
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
    </div>
    <br>
    @endif
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              
              <!-- /.card-header -->
              <div class="card-body">
                <table id="productTable" class="table table-bordered table-hover product-list">
                  <thead>
                  <tr>
                    <th>SI NO</th>
                    <th>Product Name</th>
                    <th>Price</th>
                    <th>Product Image</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                    <?php $a=1?>
                    @foreach($products as $product)
                  <tr>
                    <td>{{$a}}</td>
                    <td>{{$product->product_name}}</td>
                    <td>Rs. {{$product->rate}}</td>

                    <td>   <img style="height:80px;width: 100px" src="{{url('products/'.$product->product_image)}} " class="img-fluid"></td>
                    <td>
                                               <label class="switch">
                                                   @if($product->status==1)
                                                    <input type="checkbox" checked data-id="{{$product->id}}" class="stock-switch">
                                                    @else
                                                      <input type="checkbox"  data-id="{{$product->id}}" class="stock-switch">
                                                    @endif
                                                    <span class="slider round"></span>
                                                </label>
                    </td>
                    <td><div class="icon-sec"><span class="edit"><a href="{{url('dashboard/product-edit/'.$product->id)}}"><img src="{{url('/dist/img/edit.png')}}"></a></span>
                      <span class="del delete" data-route="{{url('delete-product/'.$product->id)}} "><a href="#" ><img src="{{url('/dist/img/delete.png')}}"></a></span></div></td>
                  </tr>
                  <?php $a++?>
                @endforeach
                  </tbody>
                 
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
    @include('layouts.footer')
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
@include('layouts.js')

<!-- page script -->


<script>
    $(document).ready(function() {
        $(".stock-switch").click(function(){
             var productId = $(this).data('id');
          if ($(this).is(":checked")) {
                    status = 1;
                } else if ($(this).is(":not(:checked)")) {
                    status = 0;
                }
                var host = "{{URL::to('/')}}";
                  $.ajax({

           type:'POST',

           url: host+'/update-product-stock',
                  

           data:{ "_token": "{{csrf_token()}}",product_id:productId,value:status},

           success:function(data){

             bootoast.toast({
                            message:'Status updated successfully',
                            position: 'top',
                            animationDuration: 300,
                            type:'success'
                        });
               if(data.error)
               {
             alert("something went wrong");
           }
           }

        });
        });
    });
</script>
 <script type="text/javascript">

        $('.delete').on('click', function () {
            var location = $(this).data('route');
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    window.location.href = location;
                }
            })
        });

    </script>

</body>
</html>
<script>
  $(function () {
   
    $('#productTable').DataTable({
       responsive: true
    
    });
  });
</script>