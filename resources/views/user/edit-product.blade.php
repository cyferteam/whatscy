<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>WhatsCy</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="icon" href="{{url('images/favicon.png')}}" type="image/x-icon"/>
  <!-- Font Awesome -->
   @include('layouts.stylesheet')
   </head>
 <body class="hold-transition sidebar-mini layout-fixed"><div class="wrapper">
    @include('layouts.navbar')
  <!-- Navbar -->
     
  <!-- /.navbar -->

  

     @include('layouts.user-side-menu')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Edit Product</h1>
          </div><!-- /.col -->
            @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
        <!-- Small boxes (Stat box) -->
         @if(Session::has('error'))
        
        <div id="alertmsg" class="alert alert-dismissable alert-danger">
              <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button> 
              <strong>Error</strong>: {{ Session::get('error') }}
          </div>
    @endif
      @if ($message = Session::get('success'))
     
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
    </div>
    <br>
    @endif
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content headzz">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-6 offset-md-3">
            <div class="add-product">
              <form class="formz" method="POST" action="{{url('update-product')}}"  accept-charset="utf-8" enctype="multipart/form-data" >
                 @csrf
                 <input type="hidden" name="product_id" value="{{ $product->id}}"> 
                <label>Product Name</label>
                <p><input type="text" class="form-control" name="name" value="{{ $product->product_name}}" required placeholder="Product Name"></p>
                <label>Price</label>
                <p><input type="text" class="form-control" name="rate" required value="{{ $product->rate}}" placeholder="Price"></p>
                 <label>Avilable</label>
                <p>  
                   @if($product->status==1)
                                                    <input type="checkbox" name="status" checked data-id="{{$product->id}}" class="stock-switch">
                                                    @else
                                                      <input type="checkbox"  name="status" data-id="{{$product->id}}" class="stock-switch">
                                                    @endif

                </p>
                <label>Product Image</label>

                <p><input type="file" name="fileUpload"></p>
                 <img style="height:80px;width: 100px" src="{{url('products/'.$product->product_image)}} " class="img-fluid">
                <div class="button-save mr-auto">
                
                  <a href="{{url('/dashboard')}}"><button class="cancelz"  type="button">Cancel</button></a>
                  <button class="savez" type="submit">Save</button>
                </div>
              </form>
            </div>
          </div>
        </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
     @include('layouts.footer')

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->
@include('layouts.js')

<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->

<!-- Bootstrap 4 -->

</body>
</html>
