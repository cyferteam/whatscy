<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>WhatsCy</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="icon" href="{{url('images/favicon.png')}}" type="image/x-icon"/>
  <!-- Font Awesome -->
   @include('layouts.stylesheet')
 </head>
 <body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">
    @include('layouts.navbar')
  <!-- Navbar -->
     
  <!-- /.navbar -->

  

     @include('layouts.admin-side-menu')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Payment Deatils</h1>
          </div><!-- /.col -->
            @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
        <!-- Small boxes (Stat box) -->
         @if(Session::has('error'))
        
        <div id="alertmsg" class="alert alert-dismissable alert-danger">
              <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button> 
              <strong>Error</strong>: {{ Session::get('error') }}
          </div>
    @endif
      @if ($message = Session::get('success'))
     
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
    </div>
    <br>
    @endif
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content headzz">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-6 offset-md-3">
            <div class="add-product">
         
          
                <label> Plan Id</label>
                <p>{{$result['plan_id']}}</p>
                 <label> Subscription Status</label>
                <p>{{$result['status']}}</p>
                <label>Started at</label>
                <p>
                   <?php 
            
                   echo date('M/d/Y ', $result['current_start']); 


                   ?>
                </p>
                <label>Ends at</label>
                <p>
                   <?php 
                     echo date('M/d/Y', $result['current_end']); 
                   ?>
                </p>
                 <label>Payment Method</label>
                <p>{{$result['payment_method']}}</p>
               
             
            </div>
          </div>
        </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
     @include('layouts.footer')

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->
@include('layouts.js')

<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->

<!-- Bootstrap 4 -->

</body>
</html>

<script>
    $(document).ready(function () {
    $('#add-product-form').validate({ 
        rules: {
          name: {
                required: true
            },
          user_name: {
                required: true,
            
               },
           },
         messages: {
    name: {
      required: "Please enter name ",
     
    },
  user_name: {
      required: "Please enter user name ",

      },
  }
    });
});
</script>
