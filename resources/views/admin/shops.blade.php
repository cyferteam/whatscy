<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>WhatsCy || Shops</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="icon" href="{{url('images/favicon.png')}}" type="image/x-icon"/>

  @include('layouts.stylesheet')
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->

    @include('layouts.navbar')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  
    <!-- Sidebar -->
  @include('layouts.admin-side-menu')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>All Shops</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Shops</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
       @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
        <!-- Small boxes (Stat box) -->
         @if(Session::has('error'))
        
        <div id="alertmsg" class="alert alert-dismissable alert-danger">
              <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button> 
              <strong>Error</strong>: {{ Session::get('error') }}
          </div>
    @endif
      @if ($message = Session::get('success'))
     
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
    </div>
    <br>
    @endif
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              
              <!-- /.card-header -->
              <div class="card-body">
                <table id="productTable" class="table table-bordered table-hover product-list">
                  <thead>
                  <tr>
                    <th>SI NO</th>
                    <th>Shop Name</th>
                    <th>Owner</th>
                    <th>Shp URL</th>
                    <th>Plan</th>
                    <th>Subscription</th>
                    <th>Status</th>
                    <th>Payment Details</th>
                  </tr>
                  </thead>
                  <tbody>
                    <?php $a=1?>
                    @foreach($shops as $shop)
                  <tr>
                    <td>{{$a}}</td>
                    <td>{{$shop->shop_name}}</td>
                             <td>{{$shop->name}}</td>
                    <td>{{$shop->shop_url}}</td>
                    <td>
                      {{$shop->plan_name}}
                    </td>
           <td>
            @if($shop->subscription_status==1)
            Active
            @else
            Deactive
            @endif
           </td>
         
                    <td>
                       <label class="switch">
                                                   @if($shop->shop_status==1)
                                                    <input type="checkbox" checked data-id="{{$shop->id}}" class="stock-switch">
                                                    @else
                                                      <input type="checkbox"  data-id="{{$shop->id}}" class="stock-switch">
                                                    @endif
                                                    <span class="slider round"></span>
                                                </label>
                    </td>
                    <td>
                      @if($shop->subscription_id)
                      <div class="icon-sec"><span class="edit"><a href="{{url('admin/viewpayment-details/').'/'.$shop->subscription_id}}">View payment details</a></span>
                    </div>
                    @else
                    No Subsription found
                    @endif
                  </td>
                  </tr>
                  <?php $a++?>
                @endforeach
                  </tbody>
                 
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
    @include('layouts.footer')
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
@include('layouts.js')

<!-- page script -->


<script>
    $(document).ready(function() {
        $(".stock-switch").click(function(){
             var shopId = $(this).data('id');
          if ($(this).is(":checked")) {
                    status = 1;
                } else if ($(this).is(":not(:checked)")) {
                    status = 0;
                }
                var host = "{{URL::to('/')}}";
                  $.ajax({

           type:'POST',

           url: host+'/admin/update-shop-status',
                  

           data:{ "_token": "{{csrf_token()}}",shop_id:shopId,value:status},

           success:function(data){

             bootoast.toast({
                            message:'Status updated successfully',
                            position: 'top',
                            animationDuration: 300,
                            type:'success'
                        });
               if(data.error)
               {
             alert("something went wrong");
           }
           }

        });
        });
    });
</script>
 <script type="text/javascript">

        $('.delete').on('click', function () {
            var location = $(this).data('route');
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    window.location.href = location;
                }
            })
        });

    </script>

</body>
</html>
<script>
  $(function () {
   
    $('#productTable').DataTable({
    
    });
  });
</script>