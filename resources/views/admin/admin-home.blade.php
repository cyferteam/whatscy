<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>WhatsCy</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="icon" href="{{url('images/favicon.png')}}" type="image/x-icon"/>
  <!-- Font Awesome -->
   @include('layouts.stylesheet')
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">
   @include('layouts.navbar')
  
  <!-- /.navbar -->

  
    <!-- Sidebar -->
       @include('layouts.admin-side-menu')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Home</h1>
          </div><!-- /.col -->
          
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content headzz">
      <div class="container-fluid">

        <!-- Small boxes (Stat box) -->
        <div class="top-sec">
          <h2>Hi {{Auth::user()->name}},</h2>
          <h4>Welcome to your WhatsCy Dashboard</h4>
        </div>
        <div class="row left-right-val">
          <div class="col-lg-3 col-6">
            <div class="small-box" style="background: #DB5D63 !important;">
              <div class="inner">
                <div class="icon">
                <img src="{{url('dist/img/icon-1.png')}}" class="img-fluid">
              </div>
              <div class="number-sec">
                <h3>{{$productsCount}}</h3>
              </div>
              </div>
              <div class="text-secc">
                <h3>All<br>Products</h3>
              </div>
              <div class="button-sec">
              <a href="{{url('admin/view-products')}}">See More</a>
            </div>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box" style="background: #4B8FE4 !important;">
              <div class="inner">
                <div class="icon">
                <img src="{{url('dist/img/icon-2.png')}}" class="img-fluid">
                </div>
                <div class="number-sec">
                <h3>{{$orderCount}}</h3>
              </div>
              </div>
              <div class="text-secc">
                <h3>All<br>Orders</h3>
              </div>
              <div class="button-sec">
              <a href="{{url('admin/view-orders')}}">See More</a>
            </div>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box" style="background: #37C168 !important;">
              <div class="inner">
                <div class="icon">
                <img src="{{url('dist/img/icon-3.png')}}" class="img-fluid">
              </div>
              <div class="number-sec">
                <h3>{{count($customerCount)}}</h3>
              </div>
              </div>
              <div class="text-secc">
                <h3>All<br>Customers</h3>
              </div>
              <div class="button-sec">
              <a href="{{url('admin/view-all-customers')}}">See More</a>
            </div>
            </div>
          </div>
          <!-- ./col -->
           <div class="col-lg-3 col-6">
            <!-- small box -->
          <div class="small-box" style="background: #2DC1C1 !important;">
              <div class="inner">
                <div class="icon">
                <img src="{{url('dist/img/icon-3.png')}}" class="img-fluid">
              </div>
              <div class="number-sec">
                <h3>{{$shopCount}}</h3>
              </div>
              </div>
              <div class="text-secc">
                <h3>All<br>Shops</h3>
              </div>
              <div class="button-sec">
              <a href="{{url('/admin/shops')}}">See More</a>
            </div>
            </div>
          </div>
          <!-- ./col -->
        </div>
        <!-- /.row -->
        <!-- Main row -->
        
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
   @include('layouts.footer')

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
@include('layouts.js')
</body>
</html>
