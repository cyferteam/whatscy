<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>WhatsCy</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="icon" href="{{url('images/favicon.png')}}" type="image/x-icon"/>
  <!-- Font Awesome -->
    @include('layouts.stylesheet')
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
 @include('layouts.navbar')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
 
      <!-- Sidebar user panel (optional) -->
      

      <!-- Sidebar Menu -->
       @include('layouts.admin-side-menu')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Profile</h1>
          </div><!-- /.col -->
           @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
        <!-- Small boxes (Stat box) -->
         @if(Session::has('error'))
        
        <div id="alertmsg" class="alert alert-dismissable alert-danger">
              <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button> 
              <strong>Error</strong>: {{ Session::get('error') }}
          </div>
    @endif
      @if ($message = Session::get('success'))
     
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
    </div>
    <br>
    @endif
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
      
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content headzz">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-8 offset-md-2">
            <div class="tabz">
           <ul class="nav nav-tabs">
  <li class="nav-item">
    <a class="nav-link active" data-toggle="tab" href="#home">Edit Shop</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#menu1">Change Password</a>
  </li>
  
</ul>

<!-- Tab panes -->
<div class="tab-content">
 
  <div class="tab-pane container active account-set" id="home">
     <form method="POST" action="{{url('add-shop-detials')}}" accept-charset="utf-8" enctype="multipart/form-data" >
      @csrf
    <div class="row">
      <div class="col-md-6">
        <p><label> Name</label></p>
        <input type="text" class="inpp" name="shop_name" value="{{Auth::user()->name}}" disabled placeholder="Shop Name">
      </div>
      <div class="col-md-6">
        <p><label>Email</label></p>
     
           <input type="text" class="inpp" name="shop_url" value="{{Auth::user()->email}}" disabled value="" placeholder="Shop URL">
      
     
      </div>
      
    </div>
  <!-- <div class="button-area">
    <button type="submit" class="subz">Update now</button>
  </div> -->
 </form>
  </div>
  <div class="tab-pane container fade second-tab account-set" id="menu1">
    <form method="post" action="{{url('/admin/update-admin-password')}}">
        @csrf
    <div class="row">
      
      <div class="col-md-4">
        <p><label> Current Password</label></p>
        <input id="old_password" type="password" name="old_password" required class="inpp" placeholder="Enter Current Password" autofocus>
        <span>Enter Current Password</span>
      </div>
      <div class="col-md-4">
        <p><label>New Password</label></p>
        <input type="password" id="password" class="inpp" required  name="password" placeholder="Enter Password" autofocus>
        <span>Password length must be 6-15 characters long</span>
      </div>
      <div class="col-md-4">
        <p><label>Re-Enter Password</label></p>
        <input type="password" name="password_confirmation" id="password_confirmation"  required  class="inpp" placeholder="Re-Enter Password" autofocus>
        
      </div>

    </div>
    <div class="button-area">
    <button type="submit" class="subz">Change Password</button>
  </div>
</form>
  </div>
 
</div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
   @include('layouts.footer')

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
@include('layouts.js')
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
@include('layouts.js')
</body>
</html>
