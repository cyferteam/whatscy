<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>WhatsCy||Manage Admin</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="icon" href="{{url('images/favicon.png')}}" type="image/x-icon"/>

  @include('layouts.stylesheet')
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->

    @include('layouts.navbar')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  
    <!-- Sidebar -->
  @include('layouts.admin-side-menu')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Manage Admin</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Manage Admin</li>
            </ol>
          </div>
        </div>
          
      </div><!-- /.container-fluid -->
       @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
        <!-- Small boxes (Stat box) -->
         @if(Session::has('error'))
        
        <div id="alertmsg" class="alert alert-dismissable alert-danger">
              <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button> 
              <strong>Error</strong>: {{ Session::get('error') }}
          </div>
    @endif
      @if ($message = Session::get('success'))
     
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
    </div>
    <br>
    @endif
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              
              <!-- /.card-header -->
              <div class="card-body">
                <table id="productTable" class="table table-bordered table-hover product-list">
                  <thead>
                  <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Email</th>
               
                     <th>Action</th>
                  <!--   <th>Action</th> -->
                  </tr>
                  </thead>
                  <tbody>
                    <?php $a=1?>
                 @foreach($admins as $admin)
                  <tr>
                    <td>{{$a}}</td>
                    <td>{{$admin->name}}</td>
                    <td>{{$admin->email}}</td>
                       
                            
             <td><div class="icon-sec">
                      <span class="del delete" data-route=" "><a href="#" ><img src="{{url('/dist/img/delete.png')}}"></a></span></div></td> 
                  </tr>
                  <?php $a++?>
              @endforeach
                  </tbody>
                 
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
    @include('layouts.footer')
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
</body>
</html>
@include('layouts.js')

<!-- page script -->
 <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
         <form method="post" action="{{url('add-plan')}}"  accept-charset="utf-8" enctype="multipart/form-data"> 
           @csrf
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Add Plan</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       
        <div class="form-group">
                    <label for="exampleInputEmail1">Plan Name</label>
                    <input type="text" name="name" class="form-control" id="exampleInputEmail1" required="" placeholder="Enter Plan Name">
       </div>
        <div class="form-group">
                    <label for="exampleInputEmail1">Price</label>
                    <input type="text" name="price" class="form-control" id="exampleInputEmail1" required="" placeholder="Enter  Price ">
       </div>
        <div class="form-group">
                    <label for="exampleInputEmail1">Product Limit</label>
                    <input type="text" name="limit" class="form-control" id="exampleInputEmail1" required="" placeholder="Enter Product Limit">
       </div>
       <div class="form-group">
                    <label for="exampleInputEmail1">Plan Description</label>
                    <input type="text" name="description" class="form-control" id="exampleInputEmail1" required="" placeholder="Enter Product Limit">
       </div>
  
         </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
       <form>
    </div>
  </div>
</div>


<script>
    $(document).ready(function() {
        $(".plan-switch").click(function(){
             var productId = $(this).data('id');
          if ($(this).is(":checked")) {
                    status = 1;
                } else if ($(this).is(":not(:checked)")) {
                    status = 0;
                }
                var host = "{{URL::to('/')}}";
                  $.ajax({

           type:'POST',

           //url: host+'/update-product-stock',
                  

           data:{ "_token": "{{csrf_token()}}",product_id:productId,value:status},

           success:function(data){

             bootoast.toast({
                            message:'Status updated successfully',
                            position: 'top',
                            animationDuration: 300,
                            type:'success'
                        });
               if(data.error)
               {
                 alert("something went wrong");
           }
           }

        });
        });
    });
</script>
 <script type="text/javascript">

        $('.delete').on('click', function () {
            var location = $(this).data('route');
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    window.location.href = location;
                }
            })
        });

    </script>


<script>
  $(function () {
   
    $('#productTable').DataTable({
    
    });
  });
</script>