<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>WhatsCy</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
   @include('layouts.front-style')
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

  <!-- summernote -->
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="register-page" style="height: 100%; margin: 0">
<div class="wrapper">
  <div class="content-wrapper">
    <div class="container-fluid">

    <div class="whole-divz">
      <div class="left-s">
        <div class="head-secs">
          <h1>Register for an all-inclusive<br>Experience on WhatsCy</h1>
        </div>
        <img src="dist/img/phone-block.png" class="img-fluid d-block mx-auto">
      </div>
      <div class="right-s">
        <div class="right-inner">
        <div class="lo-sec">
          <img src="dist/img/whatscy-blue-logo.png" class="img-fluid mx-auto d-block">
        </div>
        <div class="reg-form">
            <form method="POST" action="{{ route('register') }}">
                @csrf
            <input type="text" class="form-control input-class" name ="name" placeholder="Full Name">
            <input type="text" class="form-control input-class" name ="email" placeholder="Email ID">
            <input type="text" class="form-control input-class" name ="phone" placeholder="Mobile number">
            <input type="text" class="form-control input-class" name ="user_name" placeholder="Username">
            <input type="text" class="form-control input-class" name ="password" placeholder="Password">
            <button type="submit" class="submitz">SEND</button>
          </form>
        </div>
      </div>
      <div class="copy">
        <p>All Rights Reserved. 2020.</p>
      </div>
    </div>
  </div>
  </div>
  </div>
</div>

@include('layouts.front-script')

</body>
</html>
