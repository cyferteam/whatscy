<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>WhatsCy</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  @include('layouts.front-style')
  <!-- summernote -->
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="register-page log-page" style="height: 100%; margin: 0">
<div class="wrapper">
  <div class="content-wrapper">
    <div class="container-fluid">

    <div class="whole-divz">
      
      <div class="right-s top-v">
        <div class="right-inner">
        <div class="lo-sec">
          <img src="dist/img/whatscy-blue-logo.png" class="img-fluid mx-auto d-block">
        </div>
        <div class="reg-form">
            <form method="POST" action="{{ route('login') }}">
            <input type="email" class="form-control input-class" name ="name" placeholder="Full Name">
            <input type="password" class="form-control input-class" name ="email" placeholder="Email ID">
            <input type="checkbox" value="lsRememberMe" id="rememberMe" class="checkz"> <label for="rememberMe" class="labelz">Remember me</label>
            <button type="submit" class="submitz">SEND</button>
            <h6>Did you <span>forget your password?</span></h6>
          </form>
        </div>
      </div>
      <div class="copy">
        <p>All Rights Reserved. 2020.</p>
      </div>
    </div>
  </div>
  </div>
  </div>
</div>

<@include('layouts.front-script')
</body>
</html>
