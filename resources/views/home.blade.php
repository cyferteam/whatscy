<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
    <link rel="icon" href="{{url('images/favicon.png')}}" type="image/x-icon"/>
  <title>WhatsCy</title>

  <!-- Bootstrap core CSS -->
  <link href="static/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="static/custom.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="static/slick/slick.css"/>
  <link rel="stylesheet" type="text/css" href="static/slick/slick-theme.css"/>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="static/aos.css"/>
</head>

<body>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark static-top headz">
    <div class="container">
      <a class="navbar-brand" href="#">
        <img src="static/images/whatscy-blue-logo.png" class="img-fluid">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="#feature">Feature
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#pricing">Pricing</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Overview</a>
          </li>
          <li class="nav-item">
            <a class="nav-link borderz hvr-sweep-to-right" href="{{url('login')}}">Login</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <!-- Page Content -->
  <div class="banner-section">
  <div class="container">
    <div class="row">
      <div class="col-md-6 flexz">
        <div class="heading-sec aos-item" data-aos="fade-up">
          <h1>Online Shopping Has Never Gotten this Easier</h1>
          <div class="buttonzz">
            <a href="#" class="butt hvr-sweep-to-right-yellow">Try 15 days free</a>
          </div>
        </div>
      </div>
      <div class="col-md-6 aos-item" data-aos="fade-up">
        <img src="static/images/right-sec.png" class="img-fluid mx-auto d-block">
      </div>
    </div>
  </div>
  </div>
  <div class="inz">
    <div class="slantz">

  </div>
  <div class="container">
  <div class="app-work aos-item" data-aos="fade-up">
    <h3 class="aos-item" data-aos="fade-up">How does the app work?</h3>
    <div class="inner-se">
      <div class="row">
        <div class="col-md-4 aos-item" data-aos="fade-up">
          <div class="first-el">
            <img src="static/images/first-icon.png" class="img-fluid mx-auto d-block">
            <h5>Sign up/Log in</h5>
          </div>
        </div>
        <div class="col-md-4 aos-item" data-aos="fade-up">
          <div class="sec-el">
            <img src="static/images/second-icon.png" class="img-fluid mx-auto d-block">
            <h5>Upload products/services</h5>
          </div>
        </div>
        <div class="col-md-4 aos-item" data-aos="fade-up">
          <div class="third-el">
             <img src="static/images/third-icon.png" class="img-fluid mx-auto d-block">
             <h5>Get orders on whatsapp</h5>
          </div>
        </div>
      </div>
    </div>
      </div>
    </div>
  </div>
  <div class="benefits-section">
    <div class="container">
      <div class="row">
        <div class="col-md-5 aos-item" data-aos="fade-up">
          <img src="static/images/man-holding-whatscy.jpg" class="img-fluid">
        </div>
        <div class="col-md-7 aos-item" data-aos="fade-up">
          <div class="rightz">
            <h3>Benefits of Using WhatsCY</h3>
          <div class="row">
            <div class="col-md-6">
              <div class="inner-para onez">
                <p>Turn WhatsApp into a business opportunity by leveraging WhatsCy</p>
              </div>
            </div>
            <div class="col-md-6">
              <div class="inner-para twoz">
                <p>Make your local store available for every WhatsApp user with WhatsCy.</p>
              </div>
            </div>
            <div class="col-md-6">
              <div class="inner-para thirdz">
                <p>Communication with customers becomes a smooth process via instant WhatsApp chat.</p>
              </div>
            </div>
            <div class="col-md-6">
              <div class="inner-para fourthz">
                <p>Targeted customers can easily navigate and shop in just 30 seconds.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>
  </div>
  <div class="business-section">
    <div class="container">
      <div class="business-inz">
      <h4 class="aos-item" data-aos="fade-up">Digital Pathway For Your Business</h4>
      <div class="row">
        <div class="col-lg-3 col-sm-6 aos-item" data-aos="fade-up">
          <div class="hiz">
            <img src="static/images/search.svg" class="img-fluid mx-auto d-block">
          </div>
          <h5>Maintain A Catalogue</h5>
          <p>You can make 300 products catalogue on your WhatsCy account and showcase it to your targeted audience.</p>
        </div>
        <div class="col-lg-3 col-sm-6 aos-item" data-aos="fade-up">
          <div class="hiz">
           <img src="static/images/flash.svg" class="img-fluid mx-auto d-block">
          </div>
           <h5>Quick Access</h5>
          <p>Integration of WhatsCy with your WhatsApp business account allows potential customers to place an order in 3 simple steps</p>
        </div>
        <div class="col-lg-3 col-sm-6 aos-item" data-aos="fade-up">
          <div class="hiz">
           <img src="static/images/smartphone.svg" class="img-fluid mx-auto d-block">
           </div>
            <h5>Direct Contact</h5>
          <p>Send a ‘Hi’ via WhatsApp number and get connected with the retailers to place orders and to track the order process that even creates a buyer-seller trust.</p>
        </div>
        <div class="col-lg-3 col-sm-6 aos-item" data-aos="fade-up">
          <div class="hiz">
           <img src="static/images/levels.svg" class="img-fluid mx-auto d-block">
          </div>
           <h5>Easy Integration</h5>
          <p>Integrating WhatsCy with WhatsApp is an easy procedure to begin your business journey right through the mobile.</p>
        </div>
      </div>
      </div>
    </div>
  </div>
  <div class="yellow-section">
    <div class="container">
      <div class="row">
        <div class="col-md-3 aos-item" data-aos="fade-up">
          <img src="static/images/yellow-section-mobile1.png" class="img-fluid">
        </div>
        <div class="col-md-9">
          <div class="inoz">
            <div class="inoz-in">
            <h3 class="aos-item" data-aos="fade-up">An All-inclusive Experience on WhatsCy</h3>
            </div>
            <p class="aos-item" data-aos="fade-up">WhatsCy is your digital resource to reach and manage target customers not available on any online channel! Make your WhatsApp work for you via WhatsCy.</p>
            <div class="row">
              <div class="col-lg-4 col-sm-12 aos-item" data-aos="fade-up">
                <div class="inner-tree">
                   <h6>Supports Local Businesses</h6>
                   <p>Plug your local store with WhatShop to show the whole catalogue to your customers. Your Sales will be increased and whole new marketing channel is opened as whatsapp broadcast</p>
                </div>
              </div>
              <div class="col-lg-4 col-sm-12 aos-item" data-aos="fade-up">
                <div class="inner-tree">
                   <h6>Safe & Convenient</h6>
                   <p>It is a safe means to order your daily essentials from the comfort of your home while maintaining social distancing.</p>
                </div>
              </div>
              <div class="col-lg-4 col-sm-12 aos-item" data-aos="fade-up">
                <div class="inner-tree">
                   <h6>Time-saving System</h6>
                   <p>No more spending time on long phone calls to take orders and making other prospects wait. WhatShop is a prompt window for customers to place their orders whenever and wherever.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="slider-sec" id="feature">
    <div class="container">
      <h2 class="aos-item" data-aos="fade-up">WhatsCY<br>Features</h2>
      <div class="center">
        <div>
          <div class="imgz">
            <img src="static/images/yellow-section-mobile.png" class="img-fluid mx-auto d-block">
            
          </div>
        </div>
        <div>
          <div class="imgz">
            <img src="static/images/yellow-section-mobile.png" class="img-fluid mx-auto d-block">
           
          </div>
        </div>
        <div>
          <div class="imgz">
            <img src="static/images/yellow-section-mobile.png" class="img-fluid mx-auto d-block">
            
          </div>
        </div>
        <div>
          <div class="imgz">
            <img src="static/images/yellow-section-mobile.png" class="img-fluid mx-auto d-block">
           
          </div>
        </div>
        <div>
          <div class="imgz">
            <img src="static/images/yellow-section-mobile.png" class="img-fluid mx-auto d-block">
           
          </div>
        </div>
        <div>
          <div class="imgz">
            <img src="static/images/yellow-section-mobile.png" class="img-fluid mx-auto d-block">
            
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="plan-section" id="pricing">
    <div class="container">
      <h2 class="aos-item" data-aos="fade-up">Choose a plan that’s right for you</h2>
      <p class="aos-item" data-aos="fade-up">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore</p>
      <div class="row">
        <div class="col-md-4 aos-item" data-aos="fade-up">
          <div class="box">
            <h3>Basic</h3>
            <p class="in-box">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod.</p>
            <h4>₹299<span>/mo</span></h4>
            <div class="white-button">
              <a href="#" class="rounded-button">Get started</a>
            </div>
            <div class="list-style">
              <ul class="clearfix">
                <li>Upto 30 Products Listing</li>
                <li>Admin Panel</li>
                <li>Order History Reports</li>
                <li>24/7 Support</li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-md-4 spe-white aos-item" data-aos="fade-up">
          <div class="box whitez">
            <h3>Standard</h3>
            <p class="in-box">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod.</p>
            <h4>₹399<span>/mo</span></h4>
            <div class="white-button">
              <a href="#" class="rounded-button">Get started</a>
            </div>
            <div class="list-style">
              <ul class="clearfix">
                <li>Upto 70 Products</li>
                <li>Zoom Products Image</li>
                <li>Data Export Facility</li>
                <li>Benefits from Basic Plan</li>
                <li>24/7 Support</li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-md-4 aos-item" data-aos="fade-up">
          <div class="box">
            <h3>Premium</h3>
            <p class="in-box">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod.</p>
            <h4>₹699<span>/mo</span></h4>
            <div class="white-button">
              <a href="#" class="rounded-button">Get started</a>
            </div>
            <div class="list-style">
              <ul class="clearfix">
                <li>Upto 300 Products Listing</li>
                <li>Digital Marketing Support</li>
                <li>Zoom Products Image</li>
                <li>Benefits from Standard Plan</li>
                <li>24/7 Support</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="testimonial-section aos-item" data-aos="fade-up">
    <div class="container">
      <h2 class="aos-item" data-aos="fade-up">What our users say about us</h2>
    <div class="your-class">
    <div>
      <div class="photz">
          <img src="static/images/icon-01.png" class="img-fluid">
        </div>

      <div class="inner-box">
        <div class="text-sz">
          <h4>Bilal John, <span>Kochi</span></h4>
          <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et.</p>
        </div>
      </div>
    </div>
    <div>
      <div class="photz">
          <img src="static/images/icon-01.png" class="img-fluid">
        </div>

      <div class="inner-box">
        <div class="text-sz">
          <h4>Bilal John, <span>Kochi</span></h4>
          <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et.</p>
        </div>
      </div>
    </div>
    <div>
      <div class="photz">
          <img src="static/images/icon-01.png" class="img-fluid">
        </div>

      <div class="inner-box">
        <div class="text-sz">
          <h4>Bilal John, <span>Kochi</span></h4>
          <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et.</p>
        </div>
      </div>
    </div>
    <div>
      <div class="photz">
          <img src="static/images/icon-01.png" class="img-fluid">
        </div>

      <div class="inner-box">
        <div class="text-sz">
          <h4>Bilal John, <span>Kochi</span></h4>
          <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et.</p>
        </div>
      </div>
    </div>
    <div>
      <div class="photz">
          <img src="images/icon-01.png" class="img-fluid">
        </div>

      <div class="inner-box">
        <div class="text-sz">
          <h4>Bilal John, <span>Kochi</span></h4>
          <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et.</p>
        </div>
      </div>
    </div>
  </div>
  <div class="buttonz-ss">
            <button class="prev1 slick-arrow"> <img src="static/images/backward.png"> </button>
          </div>
  <div class="buttonz-sn">
            <button class="next1 slick-arrow"> <img src="static/images/forward.png"> </button>
          </div>
  </div>
    </div>
<div class="accordionz faq aos-item" data-aos="fade-up">
  <div class="container">
    <h3>FAQ</h3>
    <div class="bs-example">
    <div class="accordion" id="accordionExample">
        <div class="card">
            <div class="card-header" id="headingOne">
                <h2 class="mb-0">
                    <button type="button" class="btn btn-link" data-toggle="collapse" data-target="#collapseOne"><i class="fa fa-plus"></i> Stet clita kasd gubergren, no sea takimata sanctus est?</button>                  
                </h2>
            </div>
            <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                <div class="card-body">
                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.</p>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header" id="headingTwo">
                <h2 class="mb-0">
                    <button type="button" class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo"><i class="fa fa-plus"></i> Stet clita kasd gubergren, no sea takimata sanctus est?</button>
                </h2>
            </div>
            <div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo" data-parent="#accordionExample">
                <div class="card-body">
                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.</p>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header" id="headingThree">
                <h2 class="mb-0">
                    <button type="button" class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree"><i class="fa fa-plus"></i> Stet clita kasd gubergren, no sea takimata sanctus est?</button>                     
                </h2>
            </div>
            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                <div class="card-body">
                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.</p>
                </div>
            </div>
        </div>
    </div>
</div>
  </div>
</div>
<div class="footer-section">
  <div class="container">
    <div class="row">
      <div class="col-lg-4 col-md-6 aos-item" data-aos="fade-up">
        <div class="logoz">
          <img src="static/images/white-logo-whatscy.png" class="img-fluid">
          <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo.</p>
        </div>
      </div>
      <div class="col-lg-3 col-md-6 linksz aos-item" data-aos="fade-up">
        <h4>Useful Links</h4>
        <ul class="list-unstyled">
          <li><a href="#">Feature</a></li>
          <li><a href="#">Pricing</a></li>
          <li><a href="#">Overview</a></li>
        </ul>
      </div>
      <div class="col-lg-3 col-md-6 linksz aos-item" data-aos="fade-up">
        <h4>Other Links</h4>
        <ul class="list-unstyled">
          <li><a href="#">Terms And Conditions</a></li>
          <li><a href="#">Pricing</a></li>
          <li><a href="#">Overview</a></li>
        </ul>
      </div>
      <div class="col-lg-2 col-md-6 aos-item" data-aos="fade-up">
        <h4>Follow Us On:</h4>
        <ul class="list-unstyled socialz">
          <li><a  href="#"><img src="static/images/facebook.png" class="img-fluid"></a></li>
          <li><a  href="#"><img src="static/images/twitter.png" class="img-fluid"></a></li>
          <li><a  href="#"><img src="static/images/instagram.png" class="img-fluid"></a></li>
        </ul>
      </div>
    </div>
    <div class="footer-bottom">
      <p>All Rights Reserved. 2020.</p>
    </div>
  </div>
</div>

  <!-- Bootstrap core JavaScript -->
  <script src="static/vendor/jquery/jquery.slim.min.js"></script>
  <script src="static/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script type="text/javascript" src="static/slick/jquery-1.11.0.min.js"></script>
  <script type="text/javascript" src="static/slick/jquery-migrate-1.2.1.min.js"></script>
  <script type="text/javascript" src="static/slick/slick.min.js"></script>
  <script type="text/javascript" src="static/js/aos.js"></script>
  <script type='text/javascript'>

(function()
{
  if( window.localStorage )
  {
    if( !localStorage.getItem('firstLoad') )
    {
      localStorage['firstLoad'] = true;
      window.location.reload();
    }  
    else
      localStorage.removeItem('firstLoad');
  }
})();

</script>
  <script>
      AOS.init({
        easing: 'ease-in-out-sine'
      });
    </script>
  <script type="text/javascript">
  $('.center').slick({
  centerMode: true,
  autoplay: true,
  autoplaySpeed: 2000,
  dots: true,
  centerPadding: '60px',
  slidesToShow: 5,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 3
      }
    },
    {
      breakpoint: 768,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 3
      }
    },
    {
      breakpoint: 480,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 1
      }
    }
  ]
});
  </script>
<script>
  $(document).ready(function(){
    $('.your-class').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: false,
        autoplaySpeed: 2500,
        arrows: true,
        dots: true,
        pauseOnHover: false,
        prevArrow: $('.prev1'),
        nextArrow: $('.next1'),
        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 1
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 1
            }
        }, {
            breakpoint: 520,
            settings: {
                slidesToShow: 1
            }
        }]
    });
});
 </script>
 
</body>

</html>
