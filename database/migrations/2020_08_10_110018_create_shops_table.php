<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shops', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->biginteger('user_id')->unsigned();
            $table->string('shop_name',100)->nullable();
            $table->string('shop_url',255)->nullable();
            $table->string('whatsapp_no',20)->nullable();
            $table->string('shop_logo',255)->nullable();
            $table->string('title_color',20)->nullable();
            $table->string('title_background',20)->nullable();
            $table->string('about',255)->nullable();
            $table->float('extra_charges', 8, 2)->nullable();
            $table->boolean('extra_charges_status')->default(0)->nullable();
            $table->timestamps();
             $table->foreign('user_id')
            ->references('id')->on('users')
             ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shops');
    }
}
