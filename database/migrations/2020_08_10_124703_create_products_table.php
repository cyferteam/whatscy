<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->biginteger('shop_id')->unsigned();
            $table->string('product_name','255');
            $table->float('rate', 8, 2);
            $table->string('product_image','255');
            $table->boolean('status')->default(1);
            $table->timestamps();
            $table->foreign('shop_id')
            ->references('id')->on('shops')
            ->onDelete('cascade');
         
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
