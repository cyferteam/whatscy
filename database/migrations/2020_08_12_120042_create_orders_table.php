<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->biginteger('shop_id')->unsigned();
            $table->float('total_amount', 8, 2);
           $table->float('extra_charge', 8, 2)->nullable();
           $table->string('c_name','255')->nullable();
            $table->string('c_phone','15');
            $table->string('c_address','255');
            $table->string('delivery_option','255')->nullable();
            $table->timestamps();
            $table->foreign('shop_id')
            ->references('id')->on('shops')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
