<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
     protected $fillable = [
        'shop_id','product_name','rate','product_image','status'
    ];
    public static function insertData($data){

        DB::table('products')->insert($data);
    }
}
