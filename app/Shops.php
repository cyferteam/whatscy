<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shops extends Model
{
       protected $fillable = [
        'shop_name','user_id' ,'shop_url', 'whatsapp_no','shop_logo','tax','shop_timings'
    ];



public function user()
    {
        return $this->belongsTo('App\User');
    }
    
}
