<?php

namespace App\Http\Controllers;


        // $this->path = $path;use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Shops;
use App\Products;
use Illuminate\Http\Request;
use App\Orders;
use App\OrderDetails;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
   

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function __construct()
    {
        // try {
        //     $theme = Theme::where('activated', '=', 1)->first();
        //     $path  = $theme->view_path;
        // } catch (Exception $e) {
        //     $path = 'travel';
        // }
        
        // \View::share(['path' => $path]);
    }

    public function loadView(){
        if(!Auth::check()) {
              return view('auth.login');
        }
        else{
           
             //return \Redirect::to('/admin');
        }

      

    }

    public function home(){

       
 
        return view('home');
    }

     public function getShop($param,Request $request){
       if($param=="admin")
       {
           return view('auth.login');
       }
       $shop=Shops::where('shop_url',$param)
       ->where('subscription_status',1)
       ->where('shop_status',1)
       ->first();
       if(  $shop){
          $products = Products::where('shop_id',   '=',  $shop->id) 
->select('products.rate','products.product_name','product_image','products.id') 
->where('products.status',1)
->OrderBy('id','DESC')->get();
    // if ($request->ajax()) {
    //     $view = view('data',compact('products'))->render();
    //         return response()->json(['html'=>$view]);
    //     }

        return view('welcome',compact('products','shop'));

       }
       else {
        return response()->view('404');
       }
     
    }


       public function saveOrder(Request $request){

        // $name = $request->input('items');  $amount;
   
        $s = $request->input('total');
       //$string = str_replace(' ', '-', $s);
         $a = preg_replace( '/[^0-9,"."]/', '', $s );
          $amount = str_replace(',', '', $a);
         $order=new Orders();
         $order->shop_id=$request->input('uid');
         $order->total_amount= $amount;
         $order->c_name=$request->input('name');
         $order->c_phone=$request->input('phone');
         $order->c_address=$request->input('address');
         $order->delivery_option=$request->input('pickup');
        $order->save();
         foreach ($request->input('items') as  $item) {
          $details=new OrderDetails();
          $details->order_id=$order->id;
          $details->product_name=$item['product_name'];
          $details->quantity=$item['product_quantity'];
          $details->save();

        
         }
    
        return response()->json(['user_token' =>  $order->id  ]);


    }
    public function index()
    {
       
        if (!Auth::check()) {
            return redirect()->route('login');
        }
        $user=Auth::user()->role;
             if (Auth::user()->role == "ADMIN") {
 
                return redirect('/admin/home');
           //return redirect('/flush');
       }
            if (Auth::user()->role == "CUSTOMER") {

                return redirect('/dashboard');
      
       }
        return \Redirect::to('/');
    }



}