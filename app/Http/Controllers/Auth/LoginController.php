<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use \Illuminate\Http\Request;
use App\User;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    function authenticated(Request $request, $user)
   {
 
      
        if($user->hasRole('superadmin')) {
          
//            $user->last_login =Carbon::now();
//      
//                  $user->save();
              return redirect('/admin');
           }

           elseif ($user->hasRole('ADMIN')) {
        
//               $user->last_login =Carbon::now();
//                $user->is_active =1;
//               $user->save();
                return redirect('/admin/home');
           //return redirect('/flush');
       }
          elseif ($user->hasRole('CUSTOMER')) {
//               $user->last_login =Carbon::now();
//                $user->is_active =1;
//               $user->save();
                return redirect('/dashboard');
           //return redirect('/flush');
       }
         
       //     else {
       // return redirect('/flush');
       //   }
        //$date=Carbon::now();
        
       
   }
    public function logout(Request $request)
    {
//     User::where('id',Auth::user()->id)
//                    ->update(['is_active' => 0]);
       auth()->logout();
        $request->session()->flush();
        $request->session()->regenerate();
        return redirect('/');
    }
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
