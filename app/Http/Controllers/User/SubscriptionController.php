<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Orders;
use App\Shops;
use App\Subscriptions;
use Session;
use App\Plans;
class SubscriptionController extends Controller
{
   
public function __construct()
{
    $this->middleware('customer');
    $this->key_id=  env('RAZORPAY_KEY_ID');
    $this->secret_key= env('RAZORPAY_SECRET');
}


  
   public function selectPlan(Request $request){
    $plan=$request->plan;
   $curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://$this->key_id:$this->secret_key@api.razorpay.com/v1/subscriptions",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => "plan_id=$plan&total_count=12&customer_notify=1",
  CURLOPT_HTTPHEADER => array(
    "cache-control: no-cache",
    "content-type: application/x-www-form-urlencoded",
    "postman-token: 67d92778-3ca8-ffb4-9680-c384d115f95a"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {

 $result=json_decode($response, true);
 //print_r( $result);
 $subscription_id=  $result['id'];
 $plan_id=  $result['plan_id'];
 $status=  $result['status'];
 $subscription=new Subscriptions();
 $subscription->subscription_id=$subscription_id;
 $subscription->plan_id=$plan_id;
 $subscription->user_id=Auth::user()->id;
 $subscription->status=$status;
 $subscription->save();
Session::put('subscription_id',  $subscription_id);
Session::put('plan_id', $plan_id);
Session::put('status',  $status);

 return redirect('dashboard/view-subscription');   
}


   }

  public function subscriptionPage(){

   return view('user.payment');
  }
    

    public function paymentSuccess(){
      $subscription_id=Session::get('subscription_id');
      Subscriptions::where('subscription_id',  $subscription_id)
       ->update([
           'status' => "active"
        ]);
       $shop=User::find(Auth::user()->id)->shop ;
       Shops::where('user_id',$shop['user_id'])
       ->update([
           'subscription_status' => 1
        ]);

    if( date('d') == 31 || (date('m') == 1 && date('d') > 28)){
    $date = strtotime('last day of next month');
} else {
    $date = strtotime('+1 months');
}

$next_due= date('Y-m-d', $date);
        $subscription= DB::table('subscriptions')
         ->leftjoin('plans','subscriptions.plan_id','plans.plan_id')
       ->select('plans.plan_name','subscriptions.subscription_id','plans.price')
       ->where('subscription_id',  $subscription_id)
       ->first();
        $details = [
        'title' => 'Your subscription has been confirmed.',
        'body' => 'Your subscription has been confirmed',
        'user_name'=>Auth::user()->name,
        'plan'=>$subscription->plan_name,
        'plan_id'=>$subscription_id,
        'price'=>$subscription->price,
        'paid_on'=>date('Y-m-d'),
        'next_due'=>$next_due,
    ];
   
    \Mail::to(Auth::user()->email)->send(new \App\Mail\Subscription($details));
       
    return view('user.payment-success');
         

    }
    public function paymentFailed(){
         return view('user.payment-failed');
      
    }



    public function cancelSubscription(Request $request){


       $curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://$this->key_id:$this->secret_key@api.razorpay.com/v1/subscriptions/$request->subscription_id/cancel",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_HTTPHEADER => array(
    "cache-control: no-cache",
    "content-type: application/x-www-form-urlencoded",
    "postman-token: 67d92778-3ca8-ffb4-9680-c384d115f95a"
  ),
));

$response = curl_exec($curl);

$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {

 

 $result=json_decode($response, true);


 if( $result['status']=="cancelled")
 {
   Subscriptions::where('subscription_id',  $request->subscription_id)
       ->update([
           'status' => "cancelled"
        ]);
       $shop=User::find(Auth::user()->id)->shop ;
       Shops::where('user_id',$shop['user_id'])
       ->update([
           'subscription_status' => 2
        ]);
 }

   return back()->with('success', ' successfully unsubscribed plan.');

    }
  }


public function changePlan(Request $request){
       $curl = curl_init();
$id=$this->getSubscriptionId();
$plan=$request->plan;
curl_setopt_array($curl, array(
  CURLOPT_URL => "https://$this->key_id:$this->secret_key@api.razorpay.com/v1/subscriptions/$id",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "PATCH",
  CURLOPT_POSTFIELDS => "plan_id=$plan&quantity=1&customer_notify=1",
  CURLOPT_HTTPHEADER => array(
    "cache-control: no-cache",
    "content-type: application/x-www-form-urlencoded",
    "postman-token: 67d92778-3ca8-ffb4-9680-c384d115f95a"
  ),
));

$response = curl_exec($curl);

$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {

 

 $result=json_decode($response, true);


 if( $result)
 {
   Subscriptions::where('subscription_id',$id)
       ->update([
           'plan_id' => $plan
        ]);
      
 }

   return back()->with('success', ' successfully upgraded plan.');

    }

}


public function updateSubscriptionView(){
    
    $currentPlan=$this->getCurrentPlan();
       
$plans=Plans::select('plan_id','plan_name')->whereNotIn('plan_id',[ $currentPlan])->get();
 return view('user.upgrade-plan',compact('plans'));
}

public function getCurrentPlan(){
  $subscription= DB::table('subscriptions')
         ->leftjoin('plans','subscriptions.plan_id','plans.plan_id')
       ->select('plans.plan_id')
       ->where('user_id',Auth::user()->id)
       ->first();
       return $subscription->plan_id;
}
public function getSubscriptionId(){
  $subscription= DB::table('subscriptions')
         ->leftjoin('plans','subscriptions.plan_id','plans.plan_id')
       ->select('subscriptions.subscription_id')
       ->where('user_id',Auth::user()->id)
       ->first();
       return $subscription->subscription_id;
}

  public function updateSubscription(Request $request){

       $curl = curl_init();
$id="sub_FW88l68JBv2beL";
$plan="plan_FUU2XwlyxejfgO";
curl_setopt_array($curl, array(
  CURLOPT_URL => "https://$this->key_id:$this->secret_key@api.razorpay.com/v1/subscriptions/$id",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "PATCH",
  CURLOPT_POSTFIELDS => "plan_id=$plan&quantity=1&customer_notify=1",
  CURLOPT_HTTPHEADER => array(
    "cache-control: no-cache",
    "content-type: application/x-www-form-urlencoded",
    "postman-token: 67d92778-3ca8-ffb4-9680-c384d115f95a"
  ),
));

$response = curl_exec($curl);

$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {

 

 $result=json_decode($response, true);
print_r( $result);die();

 // if( $result['status']=="cancelled")
 // {
 //   Subscriptions::where('subscription_id',  $request->subscription_id)
 //       ->update([
 //           'status' => "cancelled"
 //        ]);
 //       $shop=User::find(Auth::user()->id)->shop ;
 //       Shops::where('user_id',$shop['user_id'])
 //       ->update([
 //           'subscription_status' => 2
 //        ]);
 // }

   return back()->with('success', ' successfully unsubscribed plan.');

    }

  }


public function testPlan()
{

  $plan="plan_FUU2XwlyxejfgO";
   $curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://$this->key_id:$this->secret_key@api.razorpay.com/v1/subscriptions",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => "plan_id=$plan&total_count=12&customer_notify=1",
  CURLOPT_HTTPHEADER => array(
    "cache-control: no-cache",
    "content-type: application/x-www-form-urlencoded",
    "postman-token: 67d92778-3ca8-ffb4-9680-c384d115f95a"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {

 $result=json_decode($response, true);
 print_r( $result);die();
 $subscription_id=  $result['id'];
 $plan_id=  $result['plan_id'];
 $status=  $result['status'];
 $subscription=new Subscriptions();
 $subscription->subscription_id=$subscription_id;
 $subscription->plan_id=$plan_id;
 $subscription->user_id=Auth::user()->id;
 $subscription->status=$status;
 $subscription->save();
Session::put('subscription_id',  $subscription_id);
Session::put('plan_id', $plan_id);
Session::put('status',  $status);


}


}
