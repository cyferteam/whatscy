<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Products;
use App\Shops;
use App\Orders;
class UserDashbordController extends Controller
{
   
public function __construct()
{
    $this->middleware('customer');
}


   public function myHome()
    {
      $shop = User::find(Auth::user()->id)->shop;
  
       $productsCount=Products::where('shop_id', $shop['id'])->count();
        $orderCount=Orders::where('shop_id', $shop['id'])->count();
          $customerCount=Orders::where('shop_id', $shop['id'])->select('c_name')->get()->unique('c_name');
         $shop=Shops::where('user_id',Auth::user()->id)->first();
         if($shop==null)
         {
            $shop_status=0;
         }
         else {
             $shop_status=1;
         }
        return view('user.user-home',compact('productsCount','orderCount','customerCount','shop_status'));

       
    }



    public function changePassword(Request $request){

      if (!(Hash::check($request->get('old_password'), Auth::user()->password))) {
            // The passwords not matches
            return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
            //return response()->json(['errors' => ['current'=> ['Current password does not match']]], 422);
        }
        //uncomment this if you need to validate that the new password is same as old one

        if(strcmp($request->get('old_password'), $request->get('password')) == 0){
            //Current password and new password are same
            return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
            //return response()->json(['errors' => ['current'=> ['New Password cannot be same as your current password']]], 422);
        }
        $validatedData = $request->validate([
            'old_password' => 'required',
            'password' => 'required|confirmed',
        ]);
        //Change Password
        $user = Auth::user();
        $user->password = Hash::make($request->get('password'));
        $user->save();
       // return $user;
      return back()->with('success', 'Password changed successfully.');

    }




      public function saveProduct(Request $request)
    {
      

    
    //      $rules = [
    //     'fileUpload' =>'required|image|mimes:jpeg,png,jpg|dimensions:width=375,height=150',
    //       'text'=>'required',
    // ];

    // $customMessages = [
    //     'fileUpload.required' => 'The banner image field is required.',
    //     'text.required' => 'The banner text field is required.',
    //     'fileUpload.dimensions' => 'Image dimensions are incorrect please upload 375x150 .'
    // ];

    // $this->validate($request, $rules, $customMessages);
       // $validator = Validator::make($request->all(), [
       //     'fileUpload' => 'image|mimes:jpeg,png,jpg|max:2048|dimensions:min_width=375,min_height=150'
       // ]);
      
       if ($files = $request->file('fileUpload')) {
           $destinationPath = 'products/'; // upload path
           $profileImage = date('YmdHis') . "." . $files->getClientOriginalExtension();
           $files->move($destinationPath, $profileImage);
           $product=new Products();
           $product->shop_id=Auth::user()->id;
           $product->product_name=$request->name;
           $product->rate=$request->rate;
           $product->product_image=$profileImage;
            $product->save();
            return back()
        ->withSuccess('Great! Product added successfully');

        }
       
 
    }

      public function deleteProduct(Request $request){
       $product = Products::findOrFail($request->id);
       unlink('products/'. $product->product_image);
            $product->delete();
        return back()
        ->withSuccess('Great! Product has been deleted successfully...');

    }


    public function getCustomersInfo(){
         $shop = User::find(Auth::user()->id)->shop;
      $customers=Orders::where('shop_id', $shop['id'])->select('c_name','c_phone')->get()->unique('c_name');
        return view('user.customers',compact('customers'));
    }



    public function email(){

   $details = [
        'title' => 'Registration successfully completed on WhatsCy',
        'body' => 'Thank you for  registration',
        'name'=>'firaz',
         'user_name'=>'firaz',
           'password'=>'123',
        
    ];
    \Mail::to('firaz@cyfersolutions.com')->send(new \App\Mail\RegisterMail($details));
    }


  
    
    
}
