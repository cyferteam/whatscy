<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Auth;
use App\Products;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Shops;
class ProductsController extends Controller
{
   
public function __construct()
{
   $this->middleware('customer');
}

 public function import(Request $request) 
    {
      $limit = DB::table('subscriptions')
            ->join('plans','plans.plan_id','=','subscriptions.plan_id')
            ->select('limit')
            ->where('subscriptions.user_id',Auth::user()->id)
            ->first();
       $count = DB::table('products')
              ->join('shops','shops.id','=','products.shop_id')
              ->select(DB::raw("COUNT(*) as pcount"))
              ->where('shops.user_id',Auth::user()->id)
              ->first();
              // print_r($limit);
              // print_r($count);exit(0);
              if($limit->limit >= $count->pcount){
                if ($request->file('file') != null ){
                  // echo 'hi';
                  $file = $request->file('file');
                  $filename = $file->getClientOriginalName();
                  $extension = $file->getClientOriginalExtension();
                  $tempPath = $file->getRealPath();
                  $fileSize = $file->getSize();
                  $mimeType = $file->getMimeType();
            
                  // Valid File Extensions
                  $valid_extension = array("csv","xsl");
            
                  // 2MB in Bytes
                  $maxFileSize = 2097152; 
                  if(in_array(strtolower($extension),$valid_extension)){
                    if($fileSize <= $maxFileSize){
                      
          // File upload location
          $location = 'uploads';

          // Upload file
          $file->move($location,$filename);

          // Import CSV to Database
          $filepath = $location."/".$filename;
// print_r($filepath);exit(0);
          // Reading file
          $file = fopen($filepath,"r");

          $importData_arr = array();
          $i = 0;
          while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
            $num = count($filedata );
            for ($c=0; $c < $num; $c++) {
              $importData_arr[$i][] = $filedata [$c];
           }
           $i++;
          }
          fclose($file);
          
          
          $importData_arr = array_slice($importData_arr, 1);
          $cdata = count($importData_arr);

         $limit_scope = $limit->limit - $count->pcount;
          // print_r($importData_arr);exit(0);
        $shop=Shops::where('user_id',Auth::user()->id)->first();
          if($cdata <= $limit_scope){
            foreach($importData_arr as $importData){
              
              $insertData = array(
                "shop_id"=>$shop->id,
                "product_name"=>$importData[0],
                "rate"=>$importData[1],
                "product_image"=>$importData[2],
                'status'=>1,
               'created_at' =>now(),
               'updated_at' => now());
                Products::insertData($insertData);
 
            }
            unlink($location."/".$filename);
          }else{
            unlink($location."/".$filename);
            return back()->withError('Products limits has been exceed.. Now you can add only '.$limit_scope.' more products');
          }
                    }else{
                      return back()->withError('File Size limits 2MB in Bytes');
                    }
                  }else{
                    return back()->withError('Only CSV File');
                  }
                }
                // Excel::import(new UsersImport,request()->file('file'));
              }else{
                 unlink($location."/".$filename);
                return back()->withError('Products limits has been exceed..');
              } 
              // exit(0);
             
        return back()->withSuccess('Great! Product added successfully');
    }

   public function getShopId()
   {
     $shop=Shops::where('user_id',Auth::user()->id)->first();
     return $shop->id;
   }

public function addProduct(Request $request)
{

 $shop=Shops::where('user_id',Auth::user()->id)->first();
 if($shop['subscription_status']==1)
 {
   $checkProductLimit=$this->checkProductLimit();
    $productCount=$this->getProductCount();
  if( $checkProductLimit==$productCount)
  {
   return back()
        ->withError('Products limits has been exceed..');
  }
   

   else {

    if ($files = $request->file('fileUpload')) {
           $destinationPath = 'products/'; // upload path
           $profileImage = date('YmdHis') . "." . $files->getClientOriginalExtension();
           $files->move($destinationPath, $profileImage);
           $product=new Products();
           $product->shop_id=$this->getShopId();
           $product->product_name=$request->name;
           $product->rate=$request->rate;
           $product->product_image=$profileImage;
            $product->save();
            return back()
        ->withSuccess('Great! Product added successfully');

        }

        else {
           return back()
        ->withError('Please select a product photo');
        }

 }
 

   }
   
  else {

     return back()
        ->withError('Please Subcribe to any plan before adding product...');

  }

    
 

       

}


public function getProducts()
{
  $products=Products::where('shop_id','=',$this->getShopId())->OrderBy('id','DESC')->get();
 
   return view('user.product-list',compact('products'));
}

public function updateStock(Request $request){
Products::where('id', $request->product_id)
       ->update([
           'status' => $request->value
        ]);
}


public function deleteProduct($id){
   $product = Products::findOrFail($id);
       unlink('products/'. $product->product_image);
            $product->delete();
        return back()
        ->withSuccess('Great! Product has been deleted successfully...');
}


public function editProductView($id)
{

  $product=Products::where('id','=',$id)->first();

   return view('user.edit-product',compact('product'));

}

public function updateProduct(Request $request){

if($request->status=="on")
{
  $status=1;
}
else
{
  $status=0;
}

  Products::where('id', $request->product_id)
       ->update([
           'product_name' => $request->name,
           'rate' => $request->rate,
           'status' => $status,
]);
       if ($files = $request->file('fileUpload')) {
        $product = Products::findOrFail($request->product_id);
       unlink('products/'. $product->product_image);
          

           $destinationPath = 'products/'; // upload path
           $productImage = date('YmdHis') . "." . $files->getClientOriginalExtension();
           $files->move($destinationPath, $productImage);
            Products::where('id', $request->product_id)
       ->update([
           'product_image' => $productImage]);
           

        }
         return back()
        ->withSuccess('Great! Product added successfully');

}



public function checkProductLimit(){

  $subscription= DB::table('subscriptions')
         ->leftjoin('plans','subscriptions.plan_id','plans.plan_id')
       ->select('plans.limit')
       ->where('user_id',Auth::user()->id)
       ->first();

       return $subscription->limit;


}


public function getProductCount(){
  $products=Products::where('shop_id','=',$this->getShopId())->OrderBy('id','DESC')->count();

  return  $products;
}
     
     

  
    
    
}
