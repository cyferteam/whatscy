<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Orders;
use App\Shops;
class OrderController extends Controller
{
   
public function __construct()
{
    $this->middleware('customer');
}


   public function getOrders()
    {
     
     $shop=Shops::where('user_id',Auth::user()->id)->first();
      $orders=Orders::where('shop_id', $shop->id)->OrderBy('id','DESC')->get();

        return view('user.orders',compact('orders'));
    }
    public function getOrderDetails($id)
    {

    	 $orders=Orders::where('orders.id', $id)
    	 ->join('order_details','orders.id','=','order_details.order_id')
    	 ->get();
    	 return view('user.order-details',compact('orders'));

    }

  
    
    
}
