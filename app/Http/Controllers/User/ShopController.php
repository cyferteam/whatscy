<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Auth;
use App\User;
use Validator;
use App\Shops;
use App\Plans;
use App\Subcription;
class ShopController extends Controller
{
 public $key_id;  
 public $secret_key;
public function __construct()
{
   $this->middleware('customer');
    $this->key_id=  env('RAZORPAY_KEY_ID');
    $this->secret_key= env('RAZORPAY_SECRET');
}


   

public function getShopDetails()
{

 $shop=Shops::where('user_id',Auth::user()->id)->first();
  $plans=Plans::all();
   $result="";
  $subscription= DB::table('subscriptions')
         ->leftjoin('plans','subscriptions.plan_id','plans.plan_id')
       ->select('plans.plan_name','subscriptions.subscription_id')
       ->where('user_id',Auth::user()->id)
       ->first();
       if($subscription)
       {
        $curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://$this->key_id:$this->secret_key@api.razorpay.com/v1/subscriptions/$subscription->subscription_id",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_HTTPHEADER => array(
    "cache-control: no-cache",
    "content-type: application/x-www-form-urlencoded",
    "postman-token: 67d92778-3ca8-ffb4-9680-c384d115f95a"
  ),
));

$response = curl_exec($curl);

$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {

 $result=json_decode($response, true);

}
       }

  return view('user.account-settings',compact('shop','plans','subscription','result'));

}

public function addShopDetails(Request $request){
 

 
  
     $shop=Shops::where('user_id',Auth::user()->id)->first();
         if($shop  !== null) {
     $shop ->update(['shop_name' => $request->shop_name,
                 
                    //'shop_url' => $request->shop_url,
                     'whatsapp_no' => "+".$request->whatsapp_no,
                      'tax'=>$request->tax,
                      'shop_timings'=>$request->shop_timing
                      ]);
      if ($files = $request->file('shop_logo')) {
         $validator=  $this->validate($request, [
        'shop_logo' => 'mimes:jpeg,png|dimensions:max_width=150,max_height=60', //only allow this type extension file.
    ]);
           $destinationPath = 'shops/'; // upload path
           $shop_logo = date('YmdHis') . "." . $files->getClientOriginalExtension();
           $files->move($destinationPath,   $shop_logo );
           $shop ->update(['shop_logo' =>  $shop_logo,
                  ]);
           

        }
       return back()
        ->withSuccess('Great! Shop has been updated successfully...');
    
     } 
                 
    else {
          
          $shop = Shops::create([
      'shop_name' => $request->shop_name,
          'user_id'=>Auth::user()->id,
      'shop_url' => $request->shop_url,
      'whatsapp_no' => $request->whatsapp_no,
      'shop_logo' => $request->shop_logo,
       'tax'=>$request->tax,
      'shop_timings'=>$request->shop_timing

      
    ]);
           if ($files = $request->file('shop_logo')) {
              $validator=  $this->validate($request, [
        'shop_logo' => 'mimes:jpeg,png|dimensions:max_width=150,max_height=60', //only allow this type extension file.
    ]);
           $destinationPath = 'shops/'; // upload path
           $shop_logo = date('YmdHis') . "." . $files->getClientOriginalExtension();
           $files->move($destinationPath,   $shop_logo );
           $shop ->update(['shop_logo' =>  $shop_logo,
                  ]);
           

        }
            return back()
        ->withSuccess('Great! Shop has been updated successfully...');
        }
            

      }


     
     

  
    
    
}
