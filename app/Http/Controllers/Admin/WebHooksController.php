<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Plans;
use Razorpay\Api\Api;
class WebHooksController extends Controller
{
   
public function __construct()
{
    //$this->middleware('admin');
      $this->key_id=  env('RAZORPAY_KEY_ID');
    $this->secret_key= env('RAZORPAY_SECRET');
}


   public function event(Request $request)
    {

        $api = new Api($this->key_id,$this->secret_key);
 $json = file_get_contents('php://input');
$webhookBody = json_decode($json, true);
print_r(    $api); die();
$webhookSecret='123456';
 try {

        $webhookSignature = hash_hmac('sha256', $json, $webhookSecret);
        $api->utility->verifyWebhookSignature($json, $webhookSignature, $webhookSecret);
    
        file_put_contents('data.txt', $json);
       $plan=new Plans();
        $plan->plan_name="test";
        $plan->price=100;
        $plan->limit=4;
        $plan->description="test";
        $plan->save();

        if($webhookBody['event'] == 'order.paid') { 
             //Grant Commission - Don't want to execute it multiple times..
         }
      
        header("HTTP/1.1 200 OK"); //Sending Status to Razorpay
  } 

    catch (SignatureVerificationError $e) {
       //Invalid Signature
    }
     
       
    }


    public function addPlan(Request $request){
    	$plan=new Plans();
    	$plan->plan_name=$request->name;
    	$plan->price=$request->price;
    	$plan->limit=$request->limit;
    	$plan->description=$request->description;
    	$plan->save();
    	 return back()->with('success', 'New Plan added successfully.');
    	
    }


    
}
