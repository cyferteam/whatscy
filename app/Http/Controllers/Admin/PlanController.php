<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Plans;
class PlanController extends Controller
{
   
public function __construct()
{
    $this->middleware('admin');
}


   public function managePlans()
    {
      $plans=Plans::all();
       return view('admin.plans',compact('plans'));
       
    }


    public function addPlan(Request $request){
    	$plan=new Plans();
    	$plan->plan_name=$request->name;
    	$plan->price=$request->price;
    	$plan->limit=$request->limit;
    	$plan->description=$request->description;
    	$plan->save();
    	 return back()->with('success', 'New Plan added successfully.');
    	
    }


    
}
