<?php
 
namespace App\Http\Controllers\Admin;
 
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Banners;
use App\FAQ;
use Validator,Redirect,Response,File,DB;
 
class BannerController extends Controller
{
 
    public function loadBannerView()
    {
         $banners=Banners::all();
         return view('admin.manage-banners',compact('banners'));
    }
 
    public function save(Request $request)
    {
      

    
         $rules = [
        'fileUpload' =>'required|image|mimes:jpeg,png,jpg|dimensions:width=375,height=150',
          'text'=>'required',
    ];

    $customMessages = [
        'fileUpload.required' => 'The banner image field is required.',
        'text.required' => 'The banner text field is required.',
        'fileUpload.dimensions' => 'Image dimensions are incorrect please upload 375x150 .'
    ];

    $this->validate($request, $rules, $customMessages);
       // $validator = Validator::make($request->all(), [
       //     'fileUpload' => 'image|mimes:jpeg,png,jpg|max:2048|dimensions:min_width=375,min_height=150'
       // ]);
      
       if ($files = $request->file('fileUpload')) {
           $destinationPath = 'images/banners/'; // upload path
           $profileImage = date('YmdHis') . "." . $files->getClientOriginalExtension();
           $files->move($destinationPath, $profileImage);
           $banner=new Banners();
           $banner->banner_text=$request->text;
           $banner->banner_image=$profileImage;
          // $banner->save();
            return back()
        ->withSuccess('Great! Image has been successfully uploaded.');

        }
       
 
    }

    public function deleteBanner(Request $request){
       $banner = Banners::findOrFail($request->id);
       unlink('images/banners/'.$banner->banner_image);
       $banner->delete();
        return back()
        ->withSuccess('Great! Image has been deleted successfully uploaded.');

    }

    public function updateStatus($banner_id,$status){
      if($status==1){
        $newstatus=0;
      }
       if($status==0){
        $newstatus=1;
      }

       DB::table('banners')
             ->where('id','=',$banner_id)
             ->limit(1) 
        ->update(array('status' =>$newstatus));
         return back()->with('success', 'successfully updated.');
    }



    public function addFaq(Request $request){
      FAQ::create([
            'question' => $request->question,
            'answer'=>$request->answer
              ]);
     

        return back()->with('success', 'FAQ added successfully.');


    }

    public function manageFAQ(){
      $faqs=FAQ::all();
       return view('admin.faq',compact('faqs'));
    }
     public function deleteFaq(Request $request){
       $banner = FAQ::findOrFail($request->id);
       $banner->delete();
        return back()
        ->withSuccess('FAQ deleted successfully deleted.');

    }
}