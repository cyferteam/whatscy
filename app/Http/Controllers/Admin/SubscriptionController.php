<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Orders;
use App\Shops;
use App\Subscriptions;
use Session;
class SubscriptionController extends Controller
{
   
public function __construct()
{
    $this->middleware('admin');
    $this->key_id=  env('RAZORPAY_KEY_ID');
    $this->secret_key= env('RAZORPAY_SECRET');

}


  
   public function selectPlan(Request $request){
    $plan=$request->plan;
   $curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://$this->key_id:$this->secret_key@api.razorpay.com/v1/subscriptions",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => "plan_id=$plan&total_count=12&customer_notify=1",
  CURLOPT_HTTPHEADER => array(
    "cache-control: no-cache",
    "content-type: application/x-www-form-urlencoded",
    "postman-token: 67d92778-3ca8-ffb4-9680-c384d115f95a"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {

 $result=json_decode($response, true);
 
 $subscription_id=  $result['id'];
 $plan_id=  $result['plan_id'];
 $status=  $result['status'];
 $subscription=new Subscriptions();
 $subscription->subscription_id=$subscription_id;
 $subscription->plan_id=$plan_id;
 $subscription->user_id=Auth::user()->id;
 $subscription->status=$status;
 $subscription->save();
Session::put('subscription_id',  $subscription_id);
Session::put('plan_id', $plan_id);
Session::put('status',  $status);

 return redirect('dashboard/view-subscription');   
}


   }

  public function subscriptionPage(){

   return view('user.payment');
  }
    

    public function paymentSuccess(){
      $subscription_id=Session::get('subscription_id');
      Subscriptions::where('subscription_id',  $subscription_id)
       ->update([
           'status' => "active"
        ]);
       $shop=App\User::find(Auth::user()->id)->shop ;
       Shops::where('user_id',$shop['user_id'])
       ->update([
           'subscription_status' => 1
        ]);
         return view('user.payment-success');

    }
    public function paymentFailed(){
         return view('user.payment-failed');
      
    }




    // subscription settings


    public function getAllSubscription(){


      $curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://$this->key_id:$this->secret_key@api.razorpay.com/v1/subscriptions",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_HTTPHEADER => array(
    "cache-control: no-cache",
    "content-type: application/x-www-form-urlencoded",
    "postman-token: 67d92778-3ca8-ffb4-9680-c384d115f95a"
  ),
));

$response = curl_exec($curl);

$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {

 $result=json_decode($response, true);
 
 //echo '<pre>',print_r( $result,1),'</pre>'; die();view-payment-details
   return view('admin.view-all-subscriptions',compact('result'));
    }
    
}

public function viewPaymentDetails($id)
{

  $curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://$this->key_id:$this->secret_key@api.razorpay.com/v1/subscriptions/$id",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_HTTPHEADER => array(
    "cache-control: no-cache",
    "content-type: application/x-www-form-urlencoded",
    "postman-token: 67d92778-3ca8-ffb4-9680-c384d115f95a"
  ),
));

$response = curl_exec($curl);

$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {

 $result=json_decode($response, true);
// echo '<pre>',print_r( $result,1),'</pre>'; die();
return view('admin.view-payment-details',compact('result'));
}
}

}
