<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Products;
use App\Orders;
class ProductsController extends Controller
{
   



   public function manageProducts(Request $request)
    {
        $shops= $user= User::where('role','CUSTOMER')->get();
        
       return view('admin.add-products',compact('shops'));
       
    }


public function addShop(Request $request)
{

   $user= User::create([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'password' => Hash::make($request->password),
        ]);
      

        return back()->with('success', 'New Shop added successfully.');
}


      public function saveProduct(Request $request)
    {
      

    
    //      $rules = [
    //     'fileUpload' =>'required|image|mimes:jpeg,png,jpg|dimensions:width=375,height=150',
    //       'text'=>'required',
    // ];

    // $customMessages = [
    //     'fileUpload.required' => 'The banner image field is required.',
    //     'text.required' => 'The banner text field is required.',
    //     'fileUpload.dimensions' => 'Image dimensions are incorrect please upload 375x150 .'
    // ];

    // $this->validate($request, $rules, $customMessages);
       // $validator = Validator::make($request->all(), [
       //     'fileUpload' => 'image|mimes:jpeg,png,jpg|max:2048|dimensions:min_width=375,min_height=150'
       // ]);
      
       if ($files = $request->file('fileUpload')) {
           $destinationPath = 'products/'; // upload path
           $profileImage = date('YmdHis') . "." . $files->getClientOriginalExtension();
           $files->move($destinationPath, $profileImage);
           $product=new Products();
           $product->shop_id=$request->shop_id;
           $product->product_name=$request->name;
           $product->rate=$request->rate;
           $product->product_image=$profileImage;
            $product->save();
            return back()
        ->withSuccess('Great! Image has been successfully uploaded.');

        }
       
 
    }



    public function getProducts(){
   $products=DB::table('products')
       ->join('shops','shops.id','=','products.shop_id')
       ->select('products.product_name','products.rate','products.product_image','shops.shop_name')
       ->where('status',1)
       ->get();
       
   return view('admin.all-products',compact('products'));
}
public function getOrders(){
   $orders=DB::table('orders')
       ->join('shops','shops.id','=','orders.shop_id')
       ->select('orders.total_amount','orders.created_at','shops.shop_name')
       ->get();
       
   return view('admin.all-orders',compact('orders'));
}

 public function getCustomersInfo(){
       
      $customers=Orders::select('c_name','c_phone')->get()->unique('c_name');
        return view('admin.all-customers',compact('customers'));
    }

    
}
