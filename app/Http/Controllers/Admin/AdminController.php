<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Role;
class AdminController extends Controller
{
   

public function __construct()
{
    $this->middleware('admin');
}

   public function changePassword(Request $request){

      if (!(Hash::check($request->get('old_password'), Auth::user()->password))) {
            // The passwords not matches
            return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
            //return response()->json(['errors' => ['current'=> ['Current password does not match']]], 422);
        }
        //uncomment this if you need to validate that the new password is same as old one

        if(strcmp($request->get('old_password'), $request->get('password')) == 0){
            //Current password and new password are same
            return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
            //return response()->json(['errors' => ['current'=> ['New Password cannot be same as your current password']]], 422);
        }
        $validatedData = $request->validate([
            'old_password' => 'required',
            'password' => 'required|confirmed',
        ]);
        //Change Password
        $user = Auth::user();
        $user->password = Hash::make($request->get('password'));
        $user->save();
       // return $user;
      return back()->with('success', 'Password changed successfully.');

    }



    public function manageAdmin(){
      $admins=DB::table('users')
      ->select('name','id','email')
      ->where('role','ADMIN')
      ->get();
      return view('admin.manage-admin',compact('admins'));
    }


   public function addAdmin(Request $request){
    $rules = [
        'email' => 'unique:users',
     

    ];

    $customMessages = [
        'required' => 'The :attribute field is required.'
    ];

    $this->validate($request, $rules, $customMessages);
        $user= User::create([
            'name' => $request->name,
            'user_name' => $request->user_name,
            'email' => $request->email,
            'role' => "ADMIN",
            'password' => Hash::make($request->password),
        ]);
       $user
       ->roles()
       ->attach(Role::where('role_name', 'ADMIN')->first());
           return back()
        ->withSuccess('Admin has been updated successfully...');
   }

    
}
