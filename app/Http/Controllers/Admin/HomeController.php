<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use App\Orders;
use App\Products;
use App\Shops;
class HomeController extends Controller
{
   
public function __construct()
{
    $this->middleware('auth');
}


   public function viewHome(Request $request)
    {
        $productsCount=Products::all()->count();
        $orderCount=Orders::all()->count();
        $shopCount=Shops::all()->count();
        $customerCount=Orders::select('c_name')->get()->unique('c_name');
     
      return view('admin.admin-home',compact('productsCount','orderCount','shopCount','customerCount'));
     
    }

    



   

    
}
