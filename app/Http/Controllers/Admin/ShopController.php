<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Shops;
class ShopController extends Controller
{
   
public function __construct()
{
    $this->middleware('admin');
}


   public function manageShop()
    {
      
          $shops= DB::table('shops')
       ->join('users', 'users.id', '=', 'shops.user_id')
       ->leftjoin('subscriptions','shops.user_id','subscriptions.user_id')
       ->leftjoin('plans','subscriptions.plan_id','plans.plan_id')
       ->select('subscriptions.subscription_id','shops.shop_name','shops.shop_url','users.name','shops.shop_status','shops.subscription_status','plans.plan_name','shops.id')
       ->get();
        
       return view('admin.shops',compact('shops'));
       
    }


public function addShop(Request $request)
{

   $user= User::create([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'role' => "CUSTOMER",
            'url'=>$request->url,
            'password' => Hash::make($request->password),
        ]);
    $user
       ->roles()
       ->attach(Role::where('role_name', 'CUSTOMER')->first());
      

        return back()->with('success', 'New Shop added successfully.');
}


public function updateShopStatus(Request $request)
{
 Shops::where('id', $request->shop_id)
       ->update([
           'shop_status' => $request->value
        ]);
}


    public function manageAdmin()
    {
         $admins= $user= User::where('role','ADMIN')->get();

         return view('admin.users.manage-admins',compact('admins'));
        

    }


    public function addAdmin(Request $request){

       $user= User::create([
            'name' => $request->name,
            'email' => $request->email,
            'role' => "ADMIN",
            'password' => Hash::make($request->password),
        ]);
       $user
       ->roles()
       ->attach(Role::where('role_name', 'ADMIN')->first());

        return back()->with('success', 'New Admin successfully.');
    }

    
}
