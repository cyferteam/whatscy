<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Role;
class CustomerController extends Controller
{
   public function __construct()
{
    $this->middleware('admin');
}



   public function manageCustomers(Request $request)
    {
      $customers=DB::table('users')
                 ->select('users.*')
                 ->where('role','=','customer')
                 ->where('isVerified',1)
                 ->get();

        
       return view('admin.users.manage-customers',compact('customers'));
    }



   public function registeredUsers()
    {
      $customers=DB::table('users')
                 ->select('users.*')
                 ->where('role','=','CUSTOMER')
                 ->get();

        return view('admin.registerd-users',compact('customers'));
    }



    public function manageAdmin()
    {
      if (Gate::allows('isSuperAdmin')) {
                $admins=User::where('role', '=','admin')->Orwhere('role', '=','super_admin')->get();
        }
        else {
              $admins=User::where('role', '=','admin')->get();

        }

         return view('admin.users.manage-admins',compact('admins'));
        

    }

    
}
