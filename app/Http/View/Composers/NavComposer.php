<?php

namespace App\Http\View\Composers;

use App\User;
use DB;
class NavComposer
{
    public function compose($view)
    {
        $users=DB::table('users')
                ->select('id')
                ->get();
        $view->with('users',  $users);
    }
}