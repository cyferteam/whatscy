<?php

namespace App\Providers;

class OneSignal {

public $ids;
public $message;

		 function __construct($ids,$message) {
            $this->ids  = $ids;
            $this->message = $message;
       }
      

 public function sentNotification(){

 $ids=$this->ids;
 $message= $this->message;
$content = array(
            "en" =>  $message
            );
        
        $fields = array(
            'app_id' => "5756bf38-499c-4c5a-adf2-d228cae5330e",
            'include_player_ids' =>$ids,
            'data' => array("page_id" =>  $message),
            'contents' => $content,
             'large_icon' =>"https://plandapp.com/images/pland-large.png",
              'small_icon' =>"https://plandapp.com/images/pland-small.png",
        );
        
        $fields = json_encode($fields);
       // print("\nJSON sent:\n");
       // print($fields);
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        //print_r( $response );
        curl_close($ch);
        return ;


 }
}

