<?php

namespace App\Providers;
use App\Shops;
use Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider
{

    public function boot()
    {
        View::composer(
             'layouts.user-side-menu', 'App\Http\View\Composers\NavComposer'
        );


         View::composer('layouts.user-side-menu', function ($view) {
         $shop=Shops::where('user_id',Auth::user()->id)->first();
         if($shop==null)
         {
            $shop_status=0;
         }
         else {
             $shop_status=1;
         }
             $view->with(compact('shop_status'));
        });
         
    }


    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        // TODO: Implement register() method.
    }
}