<?php

namespace App\Imports;

use App\Products;
use App\Shops;
use Auth;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class UsersImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $shop=Shops::where('user_id',Auth::user()->id)->first();
    
       return new Products([
            'shop_id'     =>$shop->id,
            'product_name'    => $row['name'], 
            'rate' => $row['rate'], 
            'product_image' => $row['image'], 
            'status'=>1,
        ]);
    }
}
