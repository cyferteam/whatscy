<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('/test', function (Request $request) {
    return response()->json(['firaz']);
});
Route::post('register','API\RegisterController@registered');
Route::post('get-home-screen-data','API\HomeScreenController@loadHomeScreen');
Route::post('hire-gardner','API\HireGardnerController@hireGardner');
Route::post('get-orders','API\OrderController@getOrders');
Route::post('get-order-details','API\OrderController@getOrderDetails');
Route::post('verify-otp','API\LoginController@verifyOTP');
Route::post('add-user-name','API\RegisterController@addUserName');
Route::post('get-address','API\CustomerController@getAddress');
Route::post('add-address','API\CustomerController@addAddress');
Route::post('delete-address','API\CustomerController@deleteAddress');
Route::post('update-address','API\CustomerController@updateAddress');
Route::post('package-know-more','API\PackageController@packageKnowMore');
Route::post('calculate-package-price','API\PackageController@calculatePrice');
Route::post('book-package','API\PackageController@bookPackage');
Route::post('cancel-order','API\OrderController@orderCancel');
Route::get('get-faq','API\HomeScreenController@getFaqs');
Route::post('get-notifications','API\CustomerController@getNotifications');
Route::post('secure-payment-response','API\PaymentController@paymentResponse');


//packages

