<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::post('/webhooks','Admin\WebHooksController@event');
Route::get('testPlan','User\SubscriptionController@testPlan');
Route::get('/', 'HomeController@home');
 Route::get('register-whatcy', function()
    {
         return view('front.register');
    }); 
 // Route::get('u-login', function()
 //    {
 //         return view('front.u-login');
 //    }); 
//Route::get('/admin', 'Admin\AdminController@loadAdmin');
Route::get('logout','Auth\LoginController@logout');
 Route::get('dashboard','User\UserDashbordController@myHome');
Route::prefix('dashboard')->group(function () {
Route::get('products', 'User\ProductsController@getProducts');
 Route::get('/bulk-upload', function()
    {
      
         return view('user.bulk-upload');
    }); 
 

    Route::get('add-products', function()
    {
         return view('user.add-product');
    }); 
      Route::get('shop-details', function()
    {
         return view('user.shop-details');
    }); 

        Route::get('/payments', function()
    {
         return view('user.payment');
    });
Route::get('orders','User\OrderController@getOrders');
Route::get('order-details/{id}','User\OrderController@getOrderDetails');
Route::get('account-settings', 'User\ShopController@getShopDetails');
Route::get('product-edit/{id}','User\ProductsController@editProductView');
Route::post('update-user-password','User\UserDashbordController@changePassword');
Route::get('customers','User\UserDashbordController@getCustomersInfo');
// plnand and subscription
Route::post('choose-plan','User\SubscriptionController@selectPlan');
Route::post('import-products','User\ProductsController@import'); 
Route::get('subscription','User\SubscriptionController@subscriptionPage');
Route::get('view-subscription','User\SubscriptionController@subscriptionPage');
Route::get('payment-success','User\SubscriptionController@paymentSuccess');
Route::get('payment-failed','User\SubscriptionController@paymentFailed');
Route::get('unsubscribe-plan','User\SubscriptionController@cancelSubscription');
Route::get('update-plan','User\SubscriptionController@updateSubscriptionView');
Route::post('upgrade-plan','User\SubscriptionController@changePlan');
Route::get('email','User\UserDashbordController@email');

});

Route::post('/add-shop-detials','User\ShopController@addShopDetails');
Route::post('add-product','User\ProductsController@addProduct');
Route::post('update-product-stock','User\ProductsController@updateStock');
Route::get('delete-product/{id}','User\ProductsController@deleteProduct');

Route::post('update-product','User\ProductsController@updateProduct');
Route::post('ajaxRequest','HomeController@saveOrder')->name('ajaxRequest.post');




Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/{shop}', 'HomeController@getShop');
Route::get('/home', 'HomeController@index')->name('home');
Route::group(['prefix' => 'admin',  'middleware' => 'admin'], function()
{
Route::get('/home','Admin\HomeController@viewHome');
Route::get('/shops','Admin\ShopController@manageShop');
Route::get('/view-products','Admin\ProductsController@getProducts');
Route::get('/view-orders','Admin\ProductsController@getOrders');
Route::get('view-all-customers','Admin\ProductsController@getCustomersInfo');
Route::get('/plans','Admin\PlanController@managePlans');
Route::post('update-admin-password','Admin\AdminController@changePassword');
Route::get('manage-admin','Admin\AdminController@manageAdmin');
Route::get('registered-users','Admin\CustomerController@registeredUsers');
Route::post('update-shop-status','Admin\ShopController@updateShopStatus');
Route::get('getAllSubscription','Admin\SubscriptionController@getAllSubscription');
Route::get('viewpayment-details/{id}','Admin\SubscriptionController@viewPaymentDetails');

        Route::get('/add-admin', function()
    {
         return view('admin.add-admin');
    }); 
         Route::get('/profile', function()
    {
         return view('admin.profile');
    }); 

        
   
 });
Route::post('add-admin','Admin\AdminController@addAdmin');
Route::post('add-plan','Admin\PlanController@addPlan');


